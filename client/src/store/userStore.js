import { makeAutoObservable } from "mobx";
import AuthService from "../services/AuthService";
import jwtDecode from "jwt-decode";
import axios from "axios";

export default class UserStore {
  _user = {};
  _isAuth = false;

  constructor () {
    makeAutoObservable(this);
  }

  setIsAuth (bool) {
    this._isAuth = bool;
  }

  setUser (user) {
    this._user = user;
  }

  async login (email, password) {
    try {
      const response = await AuthService.login(email, password);
      const data = jwtDecode(response.data.accessToken);
      localStorage.setItem('token', response.data.accessToken);

      this.setUser(data);
      this.setIsAuth(true);
    } catch (e) {
      throw new Error(e.response.data.message);
    }
  }

  async registration (email, password, name) {
    try {
      const response = await AuthService.registration(email, password, name);
      localStorage.setItem('token', response.data.accessToken);
      console.log('response', response);

      this.setIsAuth(true);
      this.setUser(response.data.user);
    } catch (e) {
      if (e.response.data.message){
        throw new Error(e.response.data.message);
      }
    }
  }

  async logout () {
    try {
      await AuthService.logout();

      this.setIsAuth(false);
      this.setUser({});
    } catch (e) {
      if (e.response.data.message){
        console.log(e.response.data.message);
      }
    }
  }

  async check () {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API_URL}api/user/refresh`,
        {withCredentials: true});
      localStorage.setItem('token', response.data.accessToken);
      console.log('response', response);

      this.setIsAuth(true);
      this.setUser(response.data.user);
    } catch (e) {
      if (e.response.data.message){
        console.log(e.response.data.message);
      }
    }
  }
}
