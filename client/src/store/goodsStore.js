import { makeAutoObservable } from "mobx";

export default class GoodsStore {
  constructor () {
    this._selectedType = {};
    this._selectedBrand = {};

    this._types = [];

    this._brands = [];

    this._goods = [];

    // Для пагінації
    this._page = 1;
    this._countGoods =  0;
    this._limit = 8;

    makeAutoObservable(this);
  }

  setSelectedBrand (brand) {
    this._selectedBrand = brand;
  }

  setSelectedType (type) {
    this._selectedType = type;
  }

  setTypes (types) {
    this._types = types;
  }

  setBrands (brands) {
    this._brands = brands;
  }

  setGoods (goods) {
    this._goods = goods;
  }

  setPage (page) {
    this._page = page;
  }

  setLimit (limit) {
    this._limit = limit;
  }

  setCountGoods (count) {
    this._countGoods = count;
  }

  get types () {
    return this._types;
  }

  get brands () {
    return this._brands;
  }

  get goods () {
    return this._goods;
  }

  get selectedType () {
    return this._selectedType;
  }

  get selectedBrand () {
    return this._selectedBrand;
  }

  get page () {
    return this._page;
  }

  get limit () {
    return this._limit;
  }

  get countGoods () {
    return this._countGoods;
  }
}