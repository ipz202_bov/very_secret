import React, {createContext} from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import UserStore from "./store/userStore";
import GoodsStore from "./store/goodsStore";

const goods = new GoodsStore();
const user = new UserStore();

export const Context = createContext({ goods, user });

ReactDOM.render(
  <React.StrictMode>
    <Context.Provider value= {
      {
        goods: goods,
        user: user
      }
    }>
      <App />
    </Context.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
