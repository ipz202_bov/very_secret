import {authHost, host} from "../http";

export default class AuthService {
  static async login(email, password) {
    return host.post('api/user/login', {email, password} );
  }

  static async registration(email, password, name) {
    return host.post('api/user/registration', {email, password, name} );
  }

  static async logout () {
    return authHost.post('api/user/logout');
  }

}