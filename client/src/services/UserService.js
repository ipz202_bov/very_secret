import {authHost} from "../http";

export default class UserService {
  static getUsers () {
    return authHost.get('api/user/users');
  }
}