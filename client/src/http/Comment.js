import { host } from "./index";

export const postComment = async (req) => {
  const { data } = await host.post('api/comment/', req);
  return data;
}

export const getComments = async (goodsId) => {
  const { data } = await host.get(`api/comment/${goodsId}`);
  return data;
}