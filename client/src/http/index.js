import axios from "axios";

const host = axios.create( {
  withCredentials: true,
  baseURL: process.env.REACT_APP_API_URL
} );

const authHost = axios.create( {
  withCredentials: true,
  baseURL: process.env.REACT_APP_API_URL
} );

const authReqInterceptor = (config) => {
  config.headers.authorization = `Bearer ${localStorage.getItem('token')}`;
  return config;
}

const authResInterceptor = (config) => {
  return config;
}

authHost.interceptors.request.use(authReqInterceptor);
authHost.interceptors.response.use(authResInterceptor, async (error) => {
  const req = error.config;

  req._isRetry = true;

  if (error.response.status === 401 && error.config && !error.config._isRetry) {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API_URL}api/user/refresh`,
        {withCredentials: true});

      localStorage.setItem('token', response.data.accessToken);
      return authHost.request(req);
    } catch (e) {
      console.log('Користувач не авторизований!');
    }
  }

  throw error;
});

export { host, authHost }