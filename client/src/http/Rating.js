import { authHost } from "./index";

export const postRate = async (req) => {
  const { data } = await authHost.post('api/rating/', req);
  return data;
}

export const getRate = async (userId, goodsId) => {
  const { data } = await authHost.get(`api/rating/`, { params: {userId, goodsId}});
  return data;
}