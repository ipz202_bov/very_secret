import { authHost } from "./index";

export const toOrder = async (req) => {
  const { data } = await authHost.post('api/order/', req);
  return data;
}

export const getUserOrders = async (userId) => {
  const { data } = await authHost.get(`api/order/${userId}`);
  return data;
}

export const getAdminOrders = async () => {
  const { data } = await authHost.get(`api/order/all`);
  return data;
}

export const doneOrder = async (req) => {
  const { data } = await authHost.post(`api/order/done`, req);
  return data;
}

export const deleteOrder = async (req) => {
  const { data } = await authHost.post(`api/order/delete`, req);
  return data;
}