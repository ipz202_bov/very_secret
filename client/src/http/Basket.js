import { authHost } from "./index";

export const addGoodsToBasket = async (req) => {
  const { data } = await authHost.post('api/basket', req);
  return data;
}

export const getBasket = async (userId) => {
  const { data } = await authHost.get(`api/basket/${userId}`);
  return data;
}

export const delItemFromBasket = async (req) => {
  const { data } = await authHost.post(`api/basket/delete`, req);
  return data;
}

export const addCountGood = async (req) => {
  const { data } = await authHost.post(`api/basket/add`, req);
  return data;
}

export const subCountGood = async (req) => {
  const { data } = await authHost.post(`api/basket/sub`, req);
  return data;
}