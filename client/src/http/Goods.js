import { authHost, host } from "./index";

export const createType = async (type) => {
  const { data } = await authHost.post('api/type', type);
  return data;
}

export const deleteType = async (id) => {
  const { data } = await authHost.post('api/type/delete', id);
  return data;
}

export const updateType = async (type) => {
  const { data } = await authHost.post('api/type/update', type);
  return data;
}

export const getTypes = async () => {
  const { data } = await host.get('api/type');
  return data;
}

export const createBrand = async (brand) => {
  const { data } = await authHost.post('api/brand', brand);
  return data;
}

export const updateBrand = async (brand) => {
  const { data } = await authHost.post('api/brand/update', brand);
  return data;
}

export const deleteBrand = async (id) => {
  const { data } = await authHost.post('api/brand/delete', id);
  return data;
}

export const getBrands = async () => {
  const { data } = await host.get('api/brand');
  return data;
}

export const createGoods = async (goods) => {
  const { data } = await authHost.post('api/goods', goods);
  return data;
}

export const updateGoods = async (goods) => {
  const { data } = await authHost.post('/api/goods/update', goods);
  return data;
}

export const deleteGoods = async (id) => {
  const { data } = await authHost.post('api/goods/delete', id);
  return data;
}

export const getAllGoods = async (typeId, brandId, page, search) => {
  const { data } = await host.get('api/goods', { params: {
      typeId, brandId, page, search
    } });
  return data;
}

export const getOneGoods = async (id) => {
  const { data } = await host.get(`api/goods/${id}`);
  return data;
}

export const getAllForAdmin = async () => {
  const { data } = await authHost.get('api/goods/getAdmin');
  return data;
}