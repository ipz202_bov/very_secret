export const ADMIN_ROUTE = '/admin';
export const LOGIN_ROUTE = '/login';
export const REGISTRATION_ROUTE = '/registration';
export const SHOP_ROUTE = '/';
export const BASKET_ROUTE = '/basket';
export const GOODS_ROUTE = '/goods';
export const MY_ACC_ROUTE ='/my';
export const ABOUT_ROUTE = '/about';