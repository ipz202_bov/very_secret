import React, {useContext, useEffect, useState} from "react";
import { BrowserRouter } from "react-router-dom";
import { observer } from "mobx-react-lite";
import AppRouter from "./components/AppRouter";
import NavBar from "./components/NavBar";
import {Context} from "./index";
import Footer from "./components/Footer";

// CSS
import './assets/css/index.css';

const App = () => {
  const { user } = useContext(Context);

  useEffect( () => {
    if (localStorage.getItem('token')) {
      user.check();
    }
  }, []);

  return (
    <BrowserRouter>
      <NavBar/>
      <AppRouter/>
      <Footer/>
    </BrowserRouter>
  );
}

export default observer(App);
