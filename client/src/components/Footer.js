import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";

const Footer = () => {
  return (
        <Navbar bg="dark" variant="dark" className={"mt-4"} style={ { bottom: 0, width: '100%' } } fixed={"bottom"}>
          <Container>
            <Navbar.Brand href="#home">© Бубенко Олексій </Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link style={ { fontSize: 18 } } href="mailto:ipz202_bov@student.ztu.edu.ua">ipz202_bov@student.ztu.edu.ua</Nav.Link>
              <Nav.Link style={ { fontSize: 18 } } href="#">+380984627558</Nav.Link>
            </Nav>
          </Container>
        </Navbar>
  );
};

export default Footer;
