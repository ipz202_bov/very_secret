import React, { useEffect, useState } from 'react';
import {getAllGoods, deleteGoods, getAllForAdmin} from "../../http/Goods";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";

import {observer} from "mobx-react-lite";

const DeleteGoods = ( { show, onHide }  ) => {
  const [goods, setGoods] = useState([]);

  const[delGoods, setDelGoods] = useState([]);

  const findGoodsById = (id) => {
    return goods.filter( goods => goods.id === id)[0];
  }

  useEffect(() => {
    getAllForAdmin().then( (data) => setGoods(data.rows));
  }, [onHide]);


  const removeGoods = () => {
    delGoods.map( async goods => {
      try {
        await deleteGoods({id: goods.id});
      } catch (e) {
        alert(`Помилка! В категорії "${goods.name}" ще є товари! Спочатку видаліть їх для видалення категорії!`);
      }
    });

    setGoods([]);
    onHide();
  }

  const delOneGoods = (id) => {
    setDelGoods([...delGoods, findGoodsById(id) ]);
    setGoods( goods.filter(goods => goods.id !== id) );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Видалити товари
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          {
            goods.map( goods =>
              <Row className="mt-3 d-flex justify-content-center align-items-center" key={goods.id}>
                <Col md={4}>
                  <Form.Control style={ { width: '100%', textAlign: 'center' } }  placeholder="Назва"
                                value={goods.name} readOnly={true}/>
                </Col>
                <Col md={4}>
                  <Button className="mr-1" style={ { width: 100 } }
                          onClick={() => { delOneGoods(goods.id) }}
                          variant={"danger"}>Видалити</Button>
                </Col>
              </Row>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={removeGoods}>Зберегти</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(DeleteGoods);