import React, {useContext, useEffect, useState} from 'react';
import {Button, Dropdown, Modal, Row, Form, Col} from "react-bootstrap";
import {Context} from "../../index";
import {createGoods, getBrands, getTypes} from "../../http/Goods";
import {observer} from "mobx-react-lite";

const CreateGoods = observer(({ show, onHide }) => {
  const { goods } = useContext(Context);

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [count, setCount] = useState('');

  const [file, setFile] = useState(null);

  useEffect(() => {
    getTypes().then( data => goods.setTypes(data) );
    getBrands().then( data => goods.setBrands(data) );
  }, []);

  // Характеристики
  const [info, setInfo] = useState([]);

  const addInfo = () => {
    setInfo([...info, {title: '', description: '', number: Date.now()}])
  }
  const deleteInfo = (number) => {
    setInfo(info.filter(i => i.number !== number))
  }
  const changeInfo = (key, value, number) => {
    setInfo(info.map(i => i.number === number ? {...i, [key]: value} : i))
  }

  const addGoods = () => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('price', `${price}`);
    formData.append('count', `${count}`);
    formData.append('typeId', goods.selectedType.id);
    formData.append('brandId', goods.selectedBrand.id);
    formData.append('img', file[0]);

    formData.append('info', JSON.stringify(info));

    createGoods(formData).then(data => onHide());
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Додати новий товар
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Row className="d-flex align-items-center justify-content-center">
            <Dropdown style={ { width: 200 } }>
              <Dropdown.Toggle style={ { width: 200 } }>
                { goods.selectedBrand.name || `Оберіть бренд` }</Dropdown.Toggle>
              <Dropdown.Menu>
                { goods.brands.map( brand =>
                  <Dropdown.Item onClick={ () => goods.setSelectedBrand(brand) }
                                 key={ brand.id }>{ brand.name }
                  </Dropdown.Item>
                ) }
              </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={ { width: 200 } } className="m-3">
              <Dropdown.Toggle style={ { width: 200} } >
                { goods.selectedType.name || `Оберіть тип` }</Dropdown.Toggle>
              <Dropdown.Menu>
                { goods.types.map( type =>
                  <Dropdown.Item onClick={ () => goods.setSelectedType(type) }
                                 key={ type.id }>{ type.name }
                  </Dropdown.Item>
                ) }
              </Dropdown.Menu>
            </Dropdown>
          </Row>

          <Form.Control placeholder="Введіть назву товару"
                        value={name} onChange={ e => setName(e.target.value) }
                        className="mt-4"/>
          <Form.Control placeholder="Введіть ціну товару"
                        value={price}
                        onChange={ e => setPrice( Number(e.target.value)) }
                        type="number" className="mt-2"/>
          <Form.Control placeholder="Введіть кількість товару"
                        value={count}
                        onChange={ e => setCount(Number(e.target.value)) }
                        type="number" className="mt-2"/>
          <Form.Control placeholder="Оберіть світлину товару"
                        onChange={ e => setFile(e.target.files) }
                        type="file" className="mt-3"/>
          <hr/>

          <div className="d-flex justify-content-center align-items-center" >
            <Button onClick={addInfo} variant={"outline-secondary"}>
              Додати нову характеристику
            </Button>
          </div>
          {
            info.map(info =>
              <div key={info.number} className="d-flex justify-content-center align-items-center">
                <Row className="mt-3">
                  <Col md={4}>
                    <Form.Control
                      value={ info.title }
                      onChange={ (e) =>
                        changeInfo('title', e.target.value, info.number) }
                      placeholder="Назва" />
                  </Col>
                  <Col md={4}>
                    <Form.Control
                      value={ info.description }
                      onChange={ (e) =>
                        changeInfo('description', e.target.value, info.number) }
                      placeholder="Значення"/>
                  </Col>
                  <Col md={4}>
                    <Button onClick={() => deleteInfo(info.number)} variant={"danger"}>Видалити</Button>
                  </Col>
                </Row>
              </div>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <div className="d-flex"
             style={ { width: `100%`, flexDirection: 'row', justifyContent: 'space-between' } }>
          <Button variant="success" onClick={addGoods}>Додати</Button>
          <Button variant="danger" onClick={onHide}>Вийти</Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
});

export default CreateGoods;
