import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {toOrder} from "../../../http/Order";

const Order = ( { userId, totalPrice, show, onHide }  ) => {
  const [phone, setPhone] = useState('');
  const [city, setCity] = useState('');
  const [post, setPost] = useState('');

  const makeOrder = () => {
    toOrder( { userId, totalPrice, phone, city, post} )
      .then( () => { alert(`Успіх! Замовлення оформлено успішно`); onHide(); } )
      .catch( () => alert(`Помилка! Щось пішло не так, перевірте, будь ласка, введені дані!`) );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Оформити замовлення на суму { totalPrice }₴
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <span style={ { textAlign: 'center' }}>
          Будь ласка, введіть коректні дані, за якими з Вами зв'яжеться адміністратор
        </span>
        <Form>
          <Form.Control placeholder={"Введіть Ваш номер телфону"} className={"mt-1"}
                        value={phone} type={"tel"}
                        onChange={e => setPhone(e.target.value)} />
          <Form.Control placeholder={"Введіть Ваш населений пункт"} className={"mt-2"}
                        value={city}
                        onChange={e => setCity(e.target.value)} />
          <Form.Control placeholder={"Введіть Ваше поштове відділення / Поштовий індекс"}
                        className={"mt-2"}
                        value={post}
                        onChange={e => setPost(e.target.value)} />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={makeOrder}>Підтвердити</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Order;
