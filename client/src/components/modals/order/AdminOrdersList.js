import React, { useEffect, useState } from 'react';
import { getAdminOrders, doneOrder } from "../../../http/Order";

import {Button, Form, Modal, Table} from "react-bootstrap";

import {observer} from "mobx-react-lite";

const AdminOrdersList = ( { userId, show, onHide }  ) => {
  const [orders, setOrders] = useState([]);
  const [doneOrders, setDoneOrders] = useState([]);

  useEffect(() => {
    getAdminOrders().then( data => {
      setOrders(data.resultArray);
    } );
  }, [onHide]);

  const doneCheckedOrders = () => {
    doneOrders.map(order => {
        doneOrder({orderId: order.order.id} )
          .then( () => { alert('Успіх! Замовлення виконано'); onHide(); } )
          .catch( () => { alert('Помилка виконання! Виконання відмінено!'); onHide(); } )
      }
    )
  }

  const findOrderById = (id) => {
    return orders.filter(order => order.order.id === id)[0];
  }

  const doneOneOrderFromList = (orderId) => {
    setDoneOrders([...doneOrders, findOrderById(orderId)]);
    setOrders( orders.filter(o => o.order.id !== orderId) );
  }

  return (
    <Modal
      style={ { width: '100%' } }
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Історія ваших замовлень
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Table striped bordered hover style={ { fontSize: '12px', textAlign: 'center' } }>
            <tbody>
            <tr>
              <th>&#x2116; зам.</th>
              <th>&#x2116; кор.</th>
              <th>Місто</th>
              <th>Телефон</th>
              <th>Пошта</th>
              <th>Дата</th>
              <th>Товари та їх к-сть</th>
              <th>Ціна</th>
              <th>Виконано?</th>
            </tr>
            {
              orders.map(order =>
                <tr key={order.order.id}>
                  <td>{order.order.id}</td>
                  <td>{order.order.userId}</td>
                  <td>{order.order.city}</td>
                  <td>{order.order.phone}</td>
                  <td>{order.order.post}</td>
                  <td>{order.order.updatedAt.replace(/[A-Z]/g, ' ')}</td>
                  <td>{order.goods.map(goods =>
                    <div key={goods.name}>
                      <span style={ { fontStyle: 'italic', fontWeight: 'bold' } }>{ goods.name + ':'} </span>
                      <span>{ goods.count + 'шт.' }</span>
                    </div> ) }
                  </td>
                  <td>{order.order.totalPrice + '₴'}</td>
                  <td>
                    {order.order.isDone? 'ТАК' : 'НІ'}
                    {!order.order.isDone &&
                    <div className={"mt-2"} style={ { width: '100px', height: '40px' } } >
                      <Button variant={"success"}
                              onClick={ () => doneOneOrderFromList(order.order.id) }
                      >
                        Виконати
                      </Button>
                    </div>
                    }
                  </td>
                </tr>
              )
            }
            </tbody>
          </Table>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <div className="d-flex"
             style={ { width: `100%`, flexDirection: 'row', justifyContent: 'space-between' } }>
          <Button variant="success" onClick={doneCheckedOrders}>Зберегти</Button>
          <Button variant="danger" onClick={onHide}>Вийти</Button>
        </div>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(AdminOrdersList);