import React, {useState} from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import {createType} from "../../http/Goods";
import data from "bootstrap/js/src/dom/data";

const CreateType = ( { show, onHide } ) => {
  const [type, setType] = useState('');

  const addType = () => {
    createType( {name: type} ).then( data => {
      setType('');
      onHide();
    } );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Додати нову категорію
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control value={type}
                        onChange={ e => setType(e.target.value) }
                        placeholder={"Введіть назву типу"} />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={addType}>Додати</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CreateType;
