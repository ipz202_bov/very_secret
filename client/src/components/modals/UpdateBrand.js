import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {updateBrand} from "../../http/Goods";

const UpdateBrand = ( { show, onHide }  ) => {
  const [brand, setBrand] = useState('');
  const [newName, setNewName] = useState('');
  const [id, setId] = useState('');

  const upBrand = () => {
    updateBrand({ cName: brand, id: id, name: newName }).then( data => {
      setBrand('');
      setNewName('');
      setId('');
      onHide();
    })
      .catch( () => alert('Помилка! Перевірте введені парамтери!') );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Оновити бренд
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <h2>Введіть поточні назву або id бренду та нову назву</h2>
          <Form.Control placeholder={"Назва"}
                        value={brand}
                        onChange={e => setBrand(e.target.value)} />
          <Form.Control placeholder={"Id"} className="mt-2"
                        value={id}
                        onChange={e => setId(e.target.value)} />
          <Form.Control placeholder={"Нова назва бренду"} className="mt-2"
                        value={newName}
                        onChange={e => setNewName(e.target.value)} />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={upBrand}>Оновити</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UpdateBrand;
