import React, {useContext, useEffect, useState} from 'react';
import {Button, Col, Dropdown, Form, Modal, Row} from "react-bootstrap";
import {updateGoods, getOneGoods} from "../../http/Goods";
import {Context} from "../../index";
import {toJS} from "mobx";
import {observer} from "mobx-react-lite";

const UpdateGoods = ( { id, show, onHide } ) => {
  const { goods } = useContext(Context);

  const goodsJS = toJS(goods);

  console.log(goodsJS);

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [count, setCount] = useState('');
  const [info, setInfo] = useState([]);

  const [brand, setBrand]  = useState('');
  const [type, setType] = useState('');

  const [file, setFile] = useState(null);

  const fidBrandById = (id) => {
    return goodsJS._brands.filter( brand => brand.id === id)[0];
  }

  const findTypeId = (id) => {
    return goodsJS._types.filter( type => type.id === id )[0];
  }

  useEffect( () => {
    if (id !== 0) {
      getOneGoods(id).then( data => {
          setName(data.name);
          setCount(data.count);
          setPrice(data.price);
          setInfo(data.info);

          setFile(`${process.env.REACT_APP_API_URL}/${data.img}`);

          setBrand( fidBrandById(data.brandId).name );
          setType( findTypeId(data.typeId).name );
      } );
    }
  }, [id] );

  const upGoods = () => {
    const formData = new FormData();
    formData.append('id', `${id}`);
    formData.append('name', name);
    formData.append('price', `${price}`);
    formData.append('count', `${count}`);
    formData.append('typeId', type);
    formData.append('brandId', brand);

    if (file) {
      formData.append('img', file[0]);
    }

    formData.append('info', JSON.stringify(info));

    updateGoods(formData).then(() => onHide());
  }

  const deleteInfo = (number) => {
    setInfo(info.filter(i => i.id !== number));
  }
  const changeInfo = (key, value, number) => {
    console.log('NUMBER', info);
    setInfo(info.map(i => i.id === number ? {...i, [key]: value} : i));
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Оновити товар
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Row className="d-flex align-items-center justify-content-center" key={goodsJS._goods.id}>
            <Dropdown style={ { width: 200 } }>
              <Dropdown.Toggle style={ { width: 200 } }>
                { brand || `Оберіть бренд` }
              </Dropdown.Toggle>
              <Dropdown.Menu>
                { goodsJS._brands.map( brand =>
                  <Dropdown.Item onClick={ () => setBrand(brand.name) }
                                 key={ brand.id }>{ brand.name }
                  </Dropdown.Item>
                ) }
              </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={ { width: 200 } } className="m-3">
              <Dropdown.Toggle style={ { width: 200} } >
                { type || `Оберіть тип` }</Dropdown.Toggle>
              <Dropdown.Menu>
                { goodsJS._types.map( type =>
                  <Dropdown.Item onClick={ () => setType(type.name) }
                                 key={ type.id }>{ type.name }
                  </Dropdown.Item>
                ) }
              </Dropdown.Menu>
            </Dropdown>
          </Row>

          <Form.Control placeholder="Введіть назву товару"
                        value={name} onChange={ e => setName(e.target.value) }
                        className="mt-4"/>
          <Form.Control placeholder="Введіть ціну товару"
                        value={price}
                        onChange={ e => setPrice( Number(e.target.value)) }
                        type="number" className="mt-2"/>
          <Form.Control placeholder="Введіть кількість товару"
                        value={count}
                        onChange={ e => setCount(Number(e.target.value)) }
                        type="number" className="mt-2"/>
          <Form.Control placeholder="Оберіть світлину товару"
                        onChange={ e => setFile(e.target.files) }
                        type="file" className="mt-3"/>
          <hr/>

          <h2>Оновити характеристики</h2>
          {
            info.map(info =>
              <div key={info.id} className="d-flex justify-content-center align-items-center">
                <Row className="mt-3">
                  <Col md={4}>
                    <Form.Control
                      value={ info.title }
                      onChange={ (e) =>
                        changeInfo('title', e.target.value, info.id) }
                      placeholder="Назва" />
                  </Col>
                  <Col md={4}>
                    <Form.Control
                      value={ info.description }
                      onChange={ (e) =>
                        changeInfo('description', e.target.value, info.id) }
                      placeholder="Значення"/>
                  </Col>
                  <Col md={4}>
                    <Button onClick={() => deleteInfo(info.id)} variant={"danger"}>Видалити</Button>
                  </Col>
                </Row>
              </div>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <div className="d-flex"
             style={ { width: `100%`, flexDirection: 'row', justifyContent: 'space-between' } }>
          <Button variant="success" onClick={upGoods}>Оновити</Button>
          <Button variant="danger" onClick={onHide}>Вийти</Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};

export default observer(UpdateGoods);
