import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {updateType} from "../../http/Goods";

const UpdateType = ( { show, onHide }  ) => {
  const [type, setType] = useState('');
  const [newName, setNewName] = useState('');
  const [id, setId] = useState('');

  const upType = () => {
    updateType({ cName: type, id: id, name: newName }).then(data => {
      setType('');
      setNewName('');
      setId('');
      onHide();
    })
      .catch( () => alert('Помилка! Перевірте введені парамтери!') );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Оновити категорію
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <h2>Введіть поточні назву або id бренду та нову назву</h2>
          <Form.Control placeholder={"Назва"}
                        value={type}
                        onChange={e => setType(e.target.value)} />
          <Form.Control placeholder={"Id"} className="mt-2"
                        value={id}
                        onChange={e => setId(e.target.value)} />
          <Form.Control placeholder={"Нова назва бренду"} className="mt-2"
                        value={newName}
                        onChange={e => setNewName(e.target.value)} />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={upType}>Оновити</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UpdateType;