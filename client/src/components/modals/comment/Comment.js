import React, {useContext, useEffect, useState} from 'react';
import {Button, Col, Form, Modal, Row, FloatingLabel} from "react-bootstrap";

import {observer} from "mobx-react-lite";
import {postComment} from "../../../http/Comment";

const Comment = ( { show, onHide, userId, goodsId, setCommentUpdate }  ) => {
  const [comment, setComment] = useState('');

  const post = async () => {
    try {
      await postComment({ goodsId: goodsId, comment: comment, userId: userId });
      onHide();
      setComment('');
    } catch (e) {
      alert('Помилка! Коментар не може бути пустим!');
    }

    setCommentUpdate(true);
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Залиште, будь ласка, нижче відгук цьому товару
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FloatingLabel controlId="floatingTextarea2" label="Відгук">
          <Form.Control as={"textarea"} value={comment}  style={{ height: '100px' }}
                        onChange={ (e) => setComment(e.target.value) }
          >
          </Form.Control>
        </FloatingLabel>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"success"} onClick={post}>Зберегти</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(Comment);