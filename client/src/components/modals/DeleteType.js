import React, {useContext, useEffect, useState} from 'react';
import {deleteType, getTypes} from "../../http/Goods";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";

import {observer} from "mobx-react-lite";

const DeleteType = ( { show, onHide }  ) => {
  const [types, setTypes] = useState([]);
  const[delTypes, setDeleteTypes] = useState([]);

  const findTypeById = (id) => {
    return types.filter( type => type.id === id)[0];
  }

  useEffect(() => {
    getTypes().then( data => {
      setTypes(data);
    } );
  }, [onHide]);

  const removeTypes = () => {
    delTypes.map( async type => {
      try {
        await deleteType({id: type.id});
      } catch (e) {
        alert(`Помилка! В категорії "${type.name}" ще є товари! Спочатку видаліть їх для видалення категорії!`);
      }
    });

    setTypes([]);
    onHide();
  }

  const delOneType = (id) => {
    setDeleteTypes([...delTypes, findTypeById(id)]);

    setTypes( types.filter(type => type.id !== id) );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Видалити категорії
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          {
            types.map(type =>
              <Row className="mt-3 d-flex justify-content-center align-items-center" key={type.id}>
                <Col md={4}>
                  <Form.Control style={ { width: '100%', textAlign: 'center' } }  placeholder="Назва" value={type.name} readOnly={true}/>
                </Col>
                <Col md={4}>
                  <Button className="mr-1" style={ { width: 100 } }
                          onClick={() => { delOneType(type.id) }}
                          variant={"danger"}>Видалити</Button>
                </Col>
              </Row>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={removeTypes}>Зберегти</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(DeleteType);
