import React, {useContext, useEffect, useState} from 'react';
import {Button, Col, Form, Modal, Row} from "react-bootstrap";

import {observer} from "mobx-react-lite";
import {getRate, postRate} from "../../../http/Rating";

const Rate = ( { show, onHide, userId, goodsId, setUpdate }  ) => {
  const [rate, setRate] = useState(0);

  useEffect( () => {
    getRate(userId, goodsId).then( data => setRate(data.rate) );
  }, []);

  const post = async () => {
    try {
      await postRate({goodsId: goodsId, rate: rate, userId: userId})
      onHide();
      setUpdate(true);
    } catch (e) {
      alert('Помилка! Некоректне введене значення оцінки!');
    }

  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Встановіть або змініть свою оцінку цьому товару
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control type="number" value={rate} style={ { textAlign: 'center', fontSize: 40 } }
                        onChange={ (e) => setRate(e.target.value) }
          >
          </Form.Control>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"success"} onClick={post}>Зберегти</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(Rate);