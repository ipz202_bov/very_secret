import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {createBrand} from "../../http/Goods";

const CreateBrand = ( { show, onHide }  ) => {
  const [brand, setBrand] = useState('');

  const addBrand = () => {
    createBrand({name: brand}).then(data => {
      setBrand('');
      onHide();
    }).catch(() => alert('Помилка! Введені дані некоректні'));
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Додати новий бренд
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control placeholder={"Введіть назву бренду"}
                        value={brand}
                        onChange={e => setBrand(e.target.value)} />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={addBrand}>Додати</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CreateBrand;
