import React, {useContext, useEffect, useState} from 'react';
import {deleteBrand, getBrands} from "../../http/Goods";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";

import {observer} from "mobx-react-lite";

const DeleteBrand = ( { show, onHide }  ) => {
  const [brands, setBrands] = useState([]);

  const[delBrands, setDeleteBrands] = useState([]);

  const findTypeById = (id) => {
    return brands.filter( brand => brand.id === id)[0];
  }

  useEffect(() => {
    getBrands().then( data => {
      setBrands(data);
    } );
  }, [onHide]);

  const removeBrands = () => {
    delBrands.map( async brand => {
      try {
        await deleteBrand({id: brand.id});
      } catch (e) {
        alert(`Помилка! В категорії "${brand.name}" ще є товари! Спочатку видаліть їх для видалення категорії!`);
      }
    });

    setBrands([]);
    onHide();
  }

  const delOneBrand = (id) => {
    setDeleteBrands([...delBrands, findTypeById(id) ]);

    setBrands( brands.filter(brand => brand.id !== id) );
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Видалити бренди
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          {
            brands.map(brand =>
              <Row className="mt-3 d-flex justify-content-center align-items-center" key={brand.id}>
                <Col md={4}>
                  <Form.Control style={ { width: '100%', textAlign: 'center' } }  placeholder="Назва"
                                value={brand.name} readOnly={true}/>
                </Col>
                <Col md={4}>
                  <Button className="mr-1" style={ { width: 100 } }
                          onClick={() => { delOneBrand(brand.id) }}
                          variant={"danger"}>Видалити</Button>
                </Col>
              </Row>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={removeBrands}>Зберегти</Button>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(DeleteBrand);