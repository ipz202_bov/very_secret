import React, { useEffect, useState } from 'react';
import userService from "../../../services/UserService";

import {Button, Col, Form, Modal, Row} from "react-bootstrap";

import {observer} from "mobx-react-lite";

const GetUsers = ( { show, onHide }  ) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    userService.getUsers().then( data => {
      setUsers(data.data);
    } );
  }, [onHide]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Список користувачів
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Row className="mt-3 d-flex justify-content-center align-items-center" >
            <Col md={4}>
              <span style={ { width: '20px', textAlign: 'right', fontWeight: 'bold' } }>&#x2116;</span>
              <span style={ { width: '200px', textAlign: 'center', marginLeft: 20, fontWeight: 'bold' } }>
                Ім'я
              </span>
            </Col>
            <Col md={4}>
              <div style={ { width: '200px', textAlign: 'center', fontWeight: 'bold' } }>Пошта</div>
            </Col>
            <Col md={4}>
              <div style={ { width: '200px', textAlign: 'center', fontWeight: 'bold' } }>Активований?</div>
            </Col>
        </Row>
          {
            users.map( user =>
              <Row className="mt-3 d-flex justify-content-center align-items-center" key={user.id}>
                <Col md={4} >
                  <span style={ { width: '20px', textAlign: 'center' } }>{ user.id }</span>
                  <span style={ { width: '200px', textAlign: 'center', marginLeft: 20 } } >{ user.name }</span>
                </Col>
                <Col md={4}>
                  <div style={ { width: '200px', textAlign: 'center' } } >{ user.email }</div>
                </Col>
                <Col md={4}>
                  <div style={ { width: '200px', textAlign: 'center' } } >
                    { user.isActivated ? 'ТАК' : "НІ" }
                  </div>
                </Col>
              </Row>
            )
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={onHide}>Вийти</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default observer(GetUsers);