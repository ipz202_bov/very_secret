import React, {useEffect, useState} from 'react';
import {Col, Card} from "react-bootstrap";
import {getComments} from "../http/Comment";
import {observer} from "mobx-react-lite";

const CommentsList = ( { goodsId, commentUpdate, setCommentUpdate } ) => {
  const [comments, setComments] = useState([]);

  useEffect( () => {
    getComments(goodsId).then(data => setComments(data));

    setCommentUpdate(false);
  }, [commentUpdate] );

  return (
    <Col className={"mt-4"}>
      <h2>Ваші відгуки</h2>
      {
        comments.map(c =>
          <Card className={"mt-3"} style={ { padding: 4, background: 'lightgreen' } }>
            <div className={"d-flex"}
                 style={ { width: `100%`, flexDirection: 'row', justifyContent: 'space-between' } }>
              <span style={ { fontWeight: 'bold' } }>{ c.user }</span>
              <span style={ { marginLeft: 100, fontStyle: 'italic' } }>
                { c.comment.updatedAt.replace(/[A-Z]/g, ' ') }</span>
            </div>
            <div className={"mt-2"}>{ c.comment.text }</div>
          </Card>
        )
      }
    </Col>
  );
};

export default observer(CommentsList);
