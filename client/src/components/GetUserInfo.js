import React, {useContext} from 'react';
import {Col, Table, Button} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {toJS} from "mobx";

const GetUserInfo = () => {
  const { user } = useContext(Context);
  const userJS = toJS(user);

  console.log('basket', userJS);

  return (
    <Col>
      <h2 className={"mt-4"}>Інформація про акаутнт</h2>
      <Table striped bordered hover>
        <tbody>
        <tr>
          <td>
            Пошта
          </td>
          <td>
            { userJS._user.email }
          </td>
        </tr>
        <tr>
          <td>Роль</td>
          <td>
            { userJS._user.role }
          </td>
        </tr>
        <tr>
          <td>Ви активували акаунт?</td>
          <td>{ userJS._user.isActivated ? 'ТАК' : 'НІ' }</td>
        </tr>
        </tbody>
      </Table>
    </Col>
  );
};

export default observer(GetUserInfo);
