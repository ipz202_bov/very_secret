import React, {useContext } from 'react';
import { Routes, Route,  Navigate } from "react-router-dom";
import {toJS} from "mobx";
import {observer} from "mobx-react-lite";

import { SHOP_ROUTE } from "../utils/constants";
import { authRoutes, publicRoutes } from "../routes";
import { Context } from "../index";

const AppRouter = () => {
  const { user } = useContext(Context);

  const userJS = toJS(user);

  return (
    <Routes>
      { (userJS._user.role === `ADMIN` || userJS._user.role === `USER`)
        && authRoutes.map( ( { path, Component } ) =>
        <Route key={path}  path={path} element={ <Component/> }/>
      )}
      { publicRoutes.map( ( { path, Component } ) =>
        <Route key={path}  path={path} element={ <Component/> }/>
      )}
      <Route path="*" element={ <Navigate to={ SHOP_ROUTE }/> }/>
    </Routes>
  );
};

export default observer(AppRouter);
