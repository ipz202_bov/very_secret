import React, {useContext} from 'react';
import { observer } from "mobx-react-lite";
import {Button, ListGroup} from "react-bootstrap";

import {Context} from "../index";

const TypeBar = observer(( { setSearch } ) => {
  const { goods } = useContext(Context);
  return (
    <ListGroup className={"mt-1"}>
      <h2>Категорії</h2>
      { goods.types.map (type =>
        <ListGroup.Item
          style={ { cursor: 'pointer', fontSize: 20 } }
          active={type.id === goods.selectedType.id}
          key={type.id}
          onClick={ () => goods.setSelectedType(type)}
        >
          {type.name}
        </ListGroup.Item>
      )}
      <Button className={"mt-4"} variant={"success"}
              onClick={ () => { goods._selectedType = {}; goods._selectedBrand = {}; setSearch(''); } }>
        Очистити фільтри</Button>
    </ListGroup>
  );
});

export default TypeBar;
