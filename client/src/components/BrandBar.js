import React, { useContext } from 'react';
import { observer } from "mobx-react-lite";
import { Card, Row } from "react-bootstrap";

import { Context } from "../index";

const BrandBar = observer(() => {
  const { goods } = useContext(Context);

  return (
    <Row className={"mt-1"} style = { { display: 'flex', flexWrap: 'wrap' } }>
      <h2>Бренди</h2>
      <div className={"d-flex justify-content-center align-items-center"}>
        { goods.brands.map(brand =>
          <Card key={brand.id}
                onClick={ () => goods.setSelectedBrand(brand) }
                border={brand.id === goods.selectedBrand.id ? 'primary' : 'light'}
                style={ { cursor: 'pointer', width: '100px', textAlign: 'center',
                  fontSize: 20, marginLeft: 15 } }
          >
            { brand.name }
          </Card>
        ) }
      </div>
    </Row>
  );
});

export default BrandBar;
