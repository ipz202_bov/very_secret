import React, {useEffect, useState} from 'react';
import {Dropdown, Col, Button} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {getAllForAdmin} from "../http/Goods";
import UpdateGoods from "./modals/UpdateGoods";

const GoodsDropDown = () => {
  const [goods, setGoods] = useState([]);

  const [goodsUpVisible, setGoodsUpVisible] = useState(false);
  const [currentGoods, setCurrentGoods] = useState(0);

  useEffect( () => {
    getAllForAdmin().then( (data) => setGoods(data.rows));
  },[] );


  const modalOpen = (id) => {
    setGoodsUpVisible(true);
    setCurrentGoods(id);
  }

  return (
    <Col>
      <Dropdown>
        <Dropdown.Toggle variant="outline-primary" className="mt-2"
                         styleid="dropdown-basic" style={ { width: '100%' } }>
          Оновити товар
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {
            goods.map (goods =>
              <Dropdown.Item key={goods.id} >
                <Button style={ { width: '100%' } }
                        onClick={ () => modalOpen(goods.id) } >
                  <div className="d-flex align-content-center" key={goods.id}>
                    <span> {goods.id} </span>
                    <span>
                    <img src={`${process.env.REACT_APP_API_URL}${goods.img}`}
                         style={ { width: '30px', height: '30px', marginLeft: 10 } }
                         alt="Світлина товару"/>
                  </span>
                    <span style={ { marginLeft: 20 } }> {goods.name}</span>
                  </div>
                </Button>
              </Dropdown.Item>
            )
          }
        </Dropdown.Menu>

        <UpdateGoods show={goodsUpVisible} id={currentGoods}
                     onHide={ () => setGoodsUpVisible(false)}/>
      </Dropdown>
    </Col>
  );
}

export default observer(GoodsDropDown);
