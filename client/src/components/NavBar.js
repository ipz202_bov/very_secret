import React, { useContext } from 'react';
import { Context } from "../index";
import {ADMIN_ROUTE, SHOP_ROUTE, LOGIN_ROUTE, BASKET_ROUTE, MY_ACC_ROUTE, ABOUT_ROUTE} from "../utils/constants";
import { observer } from "mobx-react-lite";
import {toJS} from "mobx";
import Navbar  from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import {Image, NavLink} from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import headerPicture from "../assets/img/heder.png"


const NavBar = ()  => {
  const {user} = useContext(Context);
  const navigate = useNavigate();

  const userJS = toJS(user);

  console.log("NAVBAR",userJS);

  const logOut = () => {
    localStorage.removeItem('token');
    user.setUser({});
    user.setIsAuth(false);
  }

  return (
    <Navbar bg="dark" variant="dark" sticky="top">
      <Container>
        <NavLink href={SHOP_ROUTE}>
          <span style={ { color: 'white', fontSize: 22 } }>
              FootballHub
          </span>
        </NavLink>
        <Button variant={"outline-light"} className={"mt-2"}
                onClick={() => navigate(ABOUT_ROUTE)}>
          Про нас
        </Button>

        <div className={"d-flex align-self-end"}>
          { userJS && userJS._user.role === `ADMIN` &&
          <Button variant={"outline-light"} className="mx-2"
                  onClick={() => navigate(ADMIN_ROUTE)}>
            Адміністративна панель
          </Button>
          }
          {
            userJS && userJS._user.role === `USER` && userJS._user.isActivated &&
            <Button variant={"outline-light"} className="mr-2"
                    onClick={() => navigate(BASKET_ROUTE)}>
              Кошик
            </Button>
          }
          {
            userJS && (userJS._user.role === `USER` || userJS._user.role === `ADMIN`) &&
            <Button variant={"outline-light"} className="mx-2"
                    onClick={() => navigate(MY_ACC_ROUTE)}>
              Мій кабінет
            </Button>
          }
          { userJS && user._isAuth
            ?
            <Button variant={"outline-light"}
                    onClick={ () => logOut() }
            >
              Вийти</Button>
            :
            <Button variant={"outline-light"} onClick={ () => navigate(LOGIN_ROUTE) }>Авторизація</Button>
          }
        </div>
      </Container>
    </Navbar>
  )
};

export default observer(NavBar);
