import React from 'react';
import {Col, Card, Button, Row, Image} from "react-bootstrap";
import {addCountGood, delItemFromBasket, subCountGood} from "../../http/Basket";
import {observer} from "mobx-react-lite";

import deleteImage from "../../assets/img/delete.png"

const BasketItem = ( { goods, userId, setUpdate } ) => {
  const deleteGoods = async () => {
    await delItemFromBasket({goodsId: goods.product.id, userId: userId})
    setUpdate(true);
  }

  const addCount = async () => {
    try {
      await addCountGood({goodsId: goods.product.id, userId: userId});
    } catch (e) {
      alert(`Помилка! Задану кількість товарів неможливо встановити!`);
    }

    setUpdate(true);
  }

  const subCount = async () => {
    try {
      await subCountGood({goodsId: goods.product.id, userId: userId});
    } catch (e) {
      alert(`Помилка! Задану кількість товарів неможливо встановити!`);
    }

    setUpdate(true);
  }

  return (
      <Col className={"mt-2"}>
        <Card style={ { border: 'none', background: 'lightgreen', fontSize: 28, cursor: 'pointer' } }>
          <Row className={"d-flex align-items-center justify-content-center"}>
            <img style={ { height: '100px', width: '150px' } }
                 src={`${process.env.REACT_APP_API_URL}${goods.product.img}`} alt="Світлина товару"/>
            <div style={ { display: 'inline-block', width: '300px', textAlign: 'center' } }>{ goods.product.name }</div>

            <span style={ { width: 50, fontSize: 58, color: 'green' } }>
              <div onClick={addCount}>
              +
            </div>
            </span>

            <div style={ { display: 'inline-block', width: '70px', marginLeft: 25 } }>
              <span style={ {width: '50px', textAlign: 'center'} }>
                { goods.count }
              </span>
              </div>

            <span style={ { width: 50, fontSize: 58, color: 'red' } }>
              <div onClick={subCount}>
              -
            </div>
            </span>

            <div style={ { display: 'inline-block', width: '250px', textAlign: 'center' } }>
              Ціна: { goods.product.price * goods.count + '₴'}</div>
            <div style={ { display: 'inline-block', width: '40px' } }
                    onClick={deleteGoods}
            >
              <Image height={50} src={deleteImage}/>
            </div>
          </Row>
        </Card>
      </Col>
  );
};

export default observer(BasketItem);
