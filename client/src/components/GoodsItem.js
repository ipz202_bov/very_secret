import React, {useState} from 'react';
import {Card, Col, Image} from 'react-bootstrap';
import {useNavigate} from "react-router-dom";
import defaultImage from '../assets/img/default.png'
import ratingStar from '../assets/img/ratingStar.png';
import {GOODS_ROUTE} from "../utils/constants";
import {observer} from "mobx-react-lite";

const GoodsItem =  observer(( { goods } ) => {
  const navigate = useNavigate();

  const [image, setImage] = useState(`${process.env.REACT_APP_API_URL}${goods.img}`);

  return (
    <Col md={3} className="mt-4" onClick={ () => navigate(`${GOODS_ROUTE}/${goods.id}`) }>
      <Card style={ {width: 200, cursor: 'pointer' } } border={"light"}>
        <Image width={200} height={150}
               src={image}
               onError={ () => {
                 setImage(defaultImage);
               } }/>
        <div className="d-flex justify-content-between align-items-center">
          <div  className="d-flex"
                style={ { width: `100%`, flexDirection: 'row', justifyContent: 'space-between' } }>
            <div>{ goods.name }</div>
            <div style={ { marginTop: 20 } }>
              <div style={{display: "inline-block"}} >{ goods.rating }</div>
              <Image width={19} height={19} src={ratingStar} style={{display: "inline-block", marginTop: "-5px"}}/>
            </div>
          </div>
        </div>
      </Card>
    </Col>
  );
});

export default GoodsItem;
