import React, {useContext, useEffect, useState} from 'react';
import {Container, Col, Button} from "react-bootstrap";
import {Context} from "../index";
import {toJS} from "mobx";
import { getBasket } from "../http/Basket";
import {observer} from "mobx-react-lite";
import BasketItem from "../components/basket/BasketItem";
import Order from "../components/modals/order/Order";

const Basket = () => {
  const { user } = useContext(Context);
  const userJS = toJS(user);

  const [goods, setGoods] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  const [update, setUpdate] = useState(false);

  const [orderVisible, setOrderVisible] = useState(false);

  useEffect( async () => {
    const compare = (a, b) => {
      if (a.count > b.count)
        return -1;
      if (a.count < b.count)
        return 1;
      return 0;
    }

    if (userJS._user.id) {
      const data = await getBasket(userJS._user.id);

      setGoods(data.resultArray.sort(compare));
      setTotalPrice(data.totalPrice);

      setUpdate(false);
      }
    },[update]);

  return (
    <Container>
      <h2 className={"mt-4"}>Список обраних Вами товарів</h2>
      {
        goods.length === 0 ?
          <h5>Ваш кошик пустий</h5>
        :
          goods.map( good =>
            <div key={good.product.id}  >
              <BasketItem setUpdate={setUpdate}
                          goods={good}
                          userId={userJS._user.id}/>
            </div>,
          )
      }
      <Col className={"mt-4 d-flex"}
           style={ { width: `120%`, flexDirection: 'row', justifyContent: 'space-between', marginLeft: 'auto',
             marginRight: 'auto' } }>
        {
          goods.length !== 0 &&
          <h2>До сплати: { totalPrice + '₴' } </h2>

        }
        {
          goods.length !== 0 &&
          <Button style={ { width: '400px'} }
                onClick={ () => setOrderVisible(true) }
                variant={"outline-success"}>ОФОРМИТИ ЗАМОВЛЕННЯ</Button>
        }
      </Col>

      <Order style={ { width: '100%'} } totalPrice={totalPrice} userId={userJS._user.id}
             show={orderVisible} onHide={ () => { setOrderVisible(false); setUpdate(true); } }/>
    </Container>
  );
};

export default observer(Basket);
