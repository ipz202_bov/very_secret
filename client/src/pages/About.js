import React from 'react';
import {Container, Carousel} from "react-bootstrap";
import slider1 from "../assets/img/slider1.jpg"
import slider2 from "../assets/img/slider2.jpg"
import slider3 from "../assets/img/slider3.jpg"

const About = () => {
  return (
    <Container>
      <h2 className={"mt-4"}><div style={ {fontWeight: 'bold', fontSize: 60} }>Інтернет-магазин FootbalHub -</div>
        <div><span style={ { fontStyle: 'italic' } }>зручно, швидко, дешево</span></div></h2>
      <Carousel className={"mt-4"} style={ { width: '80%', marginLeft: 120 } }>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={slider1}
            alt="First slide"
          />
          <Carousel.Caption>
            <h3 style={ {color: '#104b50', fontSize: 42, fontWeight: 'bold', backgroundColor: 'rgba(255,255,255,.6)' } }>
              <span>Широкий асортимент продукції</span></h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={slider2}
            alt="Second slide"
          />

          <Carousel.Caption>
            <h3 style={ {color: '#104b50', fontSize: 42, fontWeight: 'bold',
              backgroundColor: 'rgba(255,255,255,.6)' } }>
              Доставка по всій Україні</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={slider3}
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3 style={ {color: '#104b50', fontSize: 42, fontWeight: 'bold',
              backgroundColor: 'rgba(255,255,255,.6)' } }>
              Цілодобова підтримка</h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <div className={"mt-5"} style={ { backgroundColor: 'white', padding: 50 } }>
      <h2 style={ {fontWeight: 'bold', fontSize: 60 } }>Про нас</h2>
        <p>Заняття спортом зараз набувають небувалу популярність, і футбол тому не виключення.
          Ця командна гра підкорила багатьох і має мільйони фанатів по всьому світу. Хтось захищає честь збірної або футбольного клубу, хтось грає в своє задоволення.
          Але для результативної та ефектною гри необхідно відчувати себе комфортно, а допоможе в цьому Інтернет-магазин футбольної екіпіровки!</p>

          <p>Не варто недооцінювати себе і недбало ставитися до спортивної форми або взуття: футбольна екіпіровка може зіграти вирішальну роль в відповідальний момент.
            Як колись влучно висловився великий футболіст Валерій Васильович Лобановський: «Футбол - це перш за все творчий процес».
            Як будь-який іменитий художник знаменитий своїм мазком пензля, екіпірування й аксесуари підкреслюють індивідуальність кожного гравця, що робить гру.</p>

          <p>Футбольний інтернет-магазин має в своєму розпорядженні тільки найякісніші товари за демократичними цінами від зарекомендувавших себе в спортивній індустрії брендів.
            Ми цінуємо кожного клієнта, тому будь-який бажаючий має можливість задати питання, висловити свою думку і ознайомитися з думками інших.
            Багатство асортименту не зможе не порадувати навіть найвимогливіших: в наявності футбольна форма та взуття, воротарське екіпірування, м'ячі та обладнання, аксесуари і
            термобілизна - перед вами відкривається чудовий футбольний світ.</p>

          <p>Зручний інтерфейс, сортування по заданих параметрах, детальна характеристика кожного товару дозволить купити футбольну екіпіровку
            буквально в один клік миші, не виходячи з дому. Ви не тільки економите час і нерви на пошуки, але і гарантовано купуєте тільки краще.</p>

        <p>Любіть футбол, реєструйтеся, шукайте те, що потрібно, купуйте, відчуєте себе справжнім футболістом, перемагайте і насолоджуйтеся грою!</p>
      </div>
    </Container>
  );
};

export default About;
