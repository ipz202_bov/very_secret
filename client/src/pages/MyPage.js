import React, {useContext, useState} from 'react';
import {Alert, Button, Container} from "react-bootstrap";
import GetUserInfo from "../components/GetUserInfo";
import UserOrdersList from "../components/modals/order/UserOrdersList";
import {Context} from "../index";
import {toJS} from "mobx";

const MyPage = () => {
  const { user } = useContext(Context);
  const userJS = toJS(user);

  const [listVisible, setListVisible] = useState(false);

  return (
    <Container>
      {
        !userJS._user.isActivated &&
        <Alert variant={"danger"}>
          Будь ласка перегляньте пошту та активуйте акаунт!
        </Alert>
      }
      {
        userJS._isAuth && userJS._user.role === `USER` && userJS._user.isActivated &&
        <h2 className={"d-flex justify-content-center mt-3"}>Замовлення</h2>
      }
      {
        userJS._isAuth && userJS._user.role === `USER` && userJS._user.isActivated &&
        <div className={"d-flex justify-content-center mt-2"} style={ { width: '100%' } }>
        <Button variant={"success"} style={ { width: '100%' } }
        onClick={ () => setListVisible(true) }
        >
        Переглянути список замовлень</Button>
        </div>
      }

      <GetUserInfo/>

      <UserOrdersList userId={userJS._user.id}
                      show={ listVisible }
                      onHide={ () => setListVisible(false) }
      />
    </Container>
  );
};

export default MyPage;
