import React, {useContext, useState} from 'react';
import {Alert, Button, Card, Container, Form, NavLink, Row} from "react-bootstrap";
import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/constants";
import {useLocation, useNavigate} from "react-router-dom";


import {observer} from "mobx-react-lite";
import {Context} from "../index";

const Auth = () => {
  const navigate = useNavigate();

  const { user } = useContext(Context);
  const location = useLocation();
  const isLogin = location.pathname === LOGIN_ROUTE;

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const authorization = async () => {
    try {
      // ЛОГІН
      if (isLogin) {
        await user.login(email, password);
      }

      // РЕЄСТРАЦІЯ
      else {
        await user.registration(email, password, name);
      }

      // Якщо код функції виконався успішно, то виконуємо переадресацію на головну сторінку
      navigate(SHOP_ROUTE);
    } catch (e) {
        alert(e.message);
      }
  }

  return (
    <Container
      className="d-flex justify-content-center align-items-center"
      style = { { height: window.innerHeight - 60 } }
    >
      <Card style={ { width: 800 } } className="p-5">
        { !isLogin &&
        <Alert variant={"success"}>
          Після реєстрації, будь ласка, активуйте акаунт на пошті
        </Alert>
        }
        <h2 className="m-auto">{ !isLogin ? 'РЕЄСТРАЦІЯ': 'ВХІД' }</h2>
        <Form className="d-flex flex-column">
          {
            !isLogin &&
              <Form.Control
                className="mt-2"
                placeholder="Введіть псевдонім..."
                type="text"
                value={name}
                onChange={ (e) => setName(e.target.value) }
              />
          }
          <Form.Control
            className="mt-2"
            placeholder="Введіть пошту..."
            type="email"
            value={email}
            onChange={ (e) => setEmail(e.target.value) }
          />
          <Form.Control
            className="mt-2"
            placeholder="Введіть пароль..."
            type="password"
            value={password}
            onChange={ (e) => setPassword(e.target.value) }
          />
        </Form>
        <Row className="d-flex justify-content-between mt-3 pl-3 pr-3">
          <div style={ { display: "flex", justifyContent:"flex-end" } }>
            <Button variant={"outline-success"}
                    onClick={authorization}
                    className="mt-2 align-self-end">
              { isLogin ? `Увійти` : `Зареєструватися` }
            </Button>
          </div>
          { isLogin
            ?
            <div>
              Немає акаунту? <NavLink style={ { display: "inline-block", padding: 0 } }
                                      href={ REGISTRATION_ROUTE }>Зареєструйтесь!</NavLink>
            </div>
            :
            <div>
              Уже є акаунт? <NavLink style={ { display: "inline-block", padding: 0 } }
                                     href={ LOGIN_ROUTE }>Увійдіть!</NavLink>
            </div>
          }
        </Row>
      </Card>
    </Container>
  );
}

export default observer(Auth);
