import React, {useContext, useEffect, useState} from 'react';
import {Col, Container, Form, Row, Button, Alert, Spinner} from "react-bootstrap";
import {observer} from "mobx-react-lite";

import TypeBar from "../components/TypeBar";
import BrandBar from "../components/BrandBar";
import GoodsList from "../components/GoodsList";
import {Context} from "../index";
import {getBrands, getAllGoods, getTypes} from "../http/Goods";
import GoodsPages from "../components/GoodsPages";
import {toJS} from "mobx";

const Shop = observer(() => {
  const { goods } = useContext(Context);
  const { user } = useContext(Context);

  const userJS = toJS(user);

  // Пустий масив deps для того, щоб хук спрацював один раз при завантаженні стоірнки
  useEffect(() => {
      getTypes().then( data => goods.setTypes(data) );
      getBrands().then( data => goods.setBrands(data) );

      getAllGoods(null, null, 1, null).then( data => {
        goods.setGoods(data.rows);
        goods.setCountGoods(data.count);
      } );

  }, []);

  useEffect(() => {
    getAllGoods( goods.selectedType.id, goods.selectedBrand.id, goods.page, null)
      .then( data => {
        goods.setGoods(data.rows);
        goods.setCountGoods(data.count);
      } )
  }, [goods.page, goods.selectedType.id, goods.selectedBrand.id]);

  const searching = () => {
    getAllGoods(null, null, 1, search).then( data => {
      goods.setGoods(data.rows);
      goods.setCountGoods(data.count);
    } );
  }

  const [search, setSearch] = useState('');

  return (
    <Container>
      <h2 className={"shop_head mt-2"}>Головна | Каталог товарів</h2>
      <Form style={ { width: '85%', display: 'inline-block'} }>
        <Form.Control value={search} placeholder={"Введіть назву товару"}
                      style={ { fontSize: 18 } }
                      onChange={ e => setSearch(e.target.value) }>
        </Form.Control>
      </Form>
      <Button onClick={searching} variant={"success"} style={ { marginLeft: 20, width: 150, fontSize: 18 } }>
        Пошук
      </Button>
      <Row className="mt-3">
        <Col md={3}>
          <TypeBar setSearch={setSearch}/>
        </Col>
        <Col md={9}>
          <BrandBar/>
          <GoodsList/>
          <div className={"d-flex justify-content-center"}>
            <GoodsPages/>
          </div>
        </Col>
      </Row>
    </Container>
  );
});

export default Shop;
