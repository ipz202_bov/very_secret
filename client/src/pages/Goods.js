import React, {useState, useEffect, useContext} from 'react';
import {useParams} from "react-router-dom";
import {Container, Col, Image, Row, Card, Button, Form, Alert} from "react-bootstrap";
import bigStar from '../assets/img/bigStar.png';
import {getOneGoods} from "../http/Goods";
import defaultImage from "../assets/img/default.png";
import {Context} from "../index";
import {toJS} from "mobx";
import {addGoodsToBasket} from "../http/Basket";
import {observer} from "mobx-react-lite";
import Rate from "../components/modals/rate/Rate";
import Comment from "../components/modals/comment/Comment";
import CommentsList from "../components/CommentsList";


const Goods = () => {
  const [goods, setGoods] = useState({info: []});
  const [image, setImage] = useState(``);
  const { id } = useParams();

  const [rateVisible, setRateVisible] = useState(false);
  const [commentUpdate, setCommentUpdate] = useState(false);
  const [comment, setComment] = useState(false);
  const [update, setUpdate] = useState(false);

  const {user} = useContext(Context);
  const userJS = toJS(user);

  const addToBasket = (userId, goodsId ) => {
    addGoodsToBasket({goodsId, userId })
      .then(() => alert(`Успіх! Товар додано до кошика`))
      .catch( () => alert(`Помилка! Товар уже є у кошику`) );
  }

  useEffect(async () => {
    setGoods(await getOneGoods(id));

    setUpdate(false);
  }, [update]);

  return (
    <Container id={"container"}>
      <Row>
        <Col md={4} style={ { marginTop: 70 } }>
          <div style={ { backgroundColor: 'white', padding: 10 } }
               className="d-flex flex-column align-items-center">
            {
              goods.id &&
              <Image style={ { height: 250 } }
                     src={`${process.env.REACT_APP_API_URL}${goods.img}`|| image}
                     onError={ () => {
                       setImage(defaultImage);
                     } }/>
            }
            <Card className="d-flex flex-column align-items-center justify-content-around priceBasketCard"
                  style={ { border: 'none', height: '30%' } }
            >
              {
                goods.id &&
                <div>
                  <h3 style={ { fontWeight: 'bold' } }>{ 'Ціна: ' + goods.price + `₴` }</h3>
                </div>
              }
              { userJS._isAuth && userJS._user.isActivated && userJS._user.role === 'USER' &&
              <Button className={"mt-3"} variant={"success"} onClick={ () => addToBasket( userJS._user.id, goods.id) }>
                Додати до кошика </Button>
              }
            </Card>
          </div>
        </Col >
        <Col md={4} className="mt-4">
          <Row className="d-flex flex-column align-items-center">
            {
              goods.id &&
              <h2 style={ { fontWeight: 'bold', fontSize: 32 } }>{ goods.name }</h2>
            }
            <div className="d-flex align-items-center justify-content-center mt-2"
                 style={ { background: `url(${bigStar}) no-repeat center center`, width: 250, height: 250,
                   backgroundCover: 'cover', fontSize: 48 } }
            >
              { goods.rating }
            </div>

            { userJS._isAuth && userJS._user.isActivated &&
            <Row>
              <Button onClick={ () => setComment(true) } className={"mt-4"} variant={"success"}>
                Залишити відгук</Button>
              <Comment show={comment} onHide={ () => setComment(false) } setCommentUpdate={setCommentUpdate}
                       goodsId={goods.id} userId={userJS._user.id}/>
            </Row>
            }

            {
              userJS._isAuth && userJS._user.isActivated && userJS._user.role === 'USER' &&
              <Row>
                <Button className={"mt-2"} variant={"success"}
                        onClick={ () => setRateVisible(true) }>
                  Встановити оцінку / Змінити оцінку </Button>
                <Rate show={rateVisible} goodsId={goods.id} userId={userJS._user.id} setUpdate={setUpdate}
                      onHide={ () => setRateVisible(false) } />
              </Row>
            }
          </Row>
        </Col>
        <Col md={4} className="mt-5">
          <Row className="d-flex flex-column">
            <h2>Характеристики</h2>
            { goods.id && goods.info.map( (info, idx) =>
              <Row key={info.id}
                   style={ { background: idx % 2 === 0 ? 'lightgreen' : 'transparent', padding: 5 } }
              >
                {info.title} : {info.description}
              </Row>
            )}
          </Row>
        </Col>
      </Row>
      {
        goods.id &&  <CommentsList goodsId={goods.id}
                                   commentUpdate={commentUpdate} setCommentUpdate={setCommentUpdate} />
      }
    </Container>
  );
};

export default observer(Goods);
