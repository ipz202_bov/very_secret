import React, {useContext, useEffect, useState} from 'react';
import {Container, Button} from "react-bootstrap";
import {observer} from "mobx-react-lite";

import CreateBrand  from "../components/modals/CreateBrand";
import CreateGoods  from "../components/modals/CreateGoods";
import CreateType  from "../components/modals/CreateType";

import DeleteType from "../components/modals/DeleteType";
import DeleteBrand from "../components/modals/DeleteBrand";
import DeleteGoods from "../components/modals/DeleteGoods";
import UpdateBrand from "../components/modals/UpdateBrand";
import UpdateType from "../components/modals/UpdateType";
import GetUsers from "../components/modals/users/GetUsers";
import GoodsDropDown from "../components/GoodsDropdown";
import AdminOrdersList from "../components/modals/order/AdminOrdersList";

const Admin = () => {
  const [usersVisible, setUsersVisible] = useState(false);

  const [brandVisible, setBrandVisible] = useState(false);
  const [typeVisible, setTypeVisible] = useState(false);
  const [goodsVisible, setGoodsVisible] = useState(false);

  const [typeDeleteVisible, setTypeDeleteVisible] = useState(false);
  const [brandDeleteVisible, setBrandDeleteVisible] = useState(false);
  const [goodsDeleteVisible, setGoodsDeleteVisible] = useState(false);

  const [brandUpVisible, setBrandUpVisible] = useState(false);
  const [typeUpVisible, setTypeUpVisible] = useState(false);

  const [ordersListVisible, setOrdersListVisible] = useState(false);

  return (
    <Container className="d-flex flex-column">
      <h2 className="mt-2">Адміністративна панель</h2>

      <Button variant={"outline-primary"} className="mt-2 p-2"
              onClick={ () => setBrandUpVisible(true)  }
      >
        Оновити бренд
      </Button>
      <Button variant={"outline-primary"} className="mt-2 p-2"
              onClick={ () => setTypeUpVisible(true)  }
      >
        Оновити категорію
      </Button>
      <GoodsDropDown />

      <Button variant={"outline-info"} className={"mt-4"}
              onClick={ () => setOrdersListVisible(true) }>
        Виконати замовлення
      </Button>
      <Button variant={"outline-success"} onClick={ () => setUsersVisible(true) } className="mt-4 p-2">
        Отримати список користувачів
      </Button>

      <Button variant={"outline-dark"} onClick={ () => setBrandVisible(true) } className="mt-4 p-2">
        Додати бренд
      </Button>
      <Button variant={"outline-dark"} onClick={ () => setTypeVisible(true) } className="mt-2 p-2">
        Додати категорію
      </Button>
      <Button variant={"outline-dark"} onClick={ () => setGoodsVisible(true) } className="mt-2 p-2">
        Додати одиницю товару
      </Button>

      <Button variant={"outline-danger"} className="mt-4 p-2"
              onClick={ () => setBrandDeleteVisible(true) }>
        Видалити бренди
      </Button>
      <Button variant={"outline-danger"} className="mt-2 p-2"
              onClick={ () => setTypeDeleteVisible(true) }>
        Видалити категорії
      </Button>
      <Button variant={"outline-danger"} className="mt-2 p-2"
              onClick={ () => setGoodsDeleteVisible(true) }>
        Видалити товари
      </Button>

      <GetUsers show={usersVisible} onHide={ () => setUsersVisible(false) } />

      <CreateBrand show={brandVisible} onHide={ () => setBrandVisible(false) }/>
      <CreateGoods show={goodsVisible} onHide={ () => setGoodsVisible(false) }/>
      <CreateType show={typeVisible} onHide={ () => setTypeVisible(false) }/>

      <DeleteType show={typeDeleteVisible} onHide={ () => setTypeDeleteVisible(false) } />
      <DeleteBrand show={brandDeleteVisible} onHide={ () => setBrandDeleteVisible(false) } />
      <DeleteGoods show={goodsDeleteVisible} onHide={ () => setGoodsDeleteVisible(false) } />

      <UpdateBrand show={brandUpVisible} onHide={ () => setBrandUpVisible(false) } />
      <UpdateType show={typeUpVisible} onHide={ () => setTypeUpVisible(false) } />

      <AdminOrdersList show={ordersListVisible} onHide={ () => setOrdersListVisible(false) } />
    </Container>
  );
};

export default observer(Admin);
