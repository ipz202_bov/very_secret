const { validationResult } = require('express-validator');

const userService = require('../services/userService');
const ApiError = require('../errors/apiError');

class UserController {
  async registration (req, res, next) {
    try {
      // Перевірка на валідність введених даних
      const errors = validationResult(req);

      // Якщо є хоча б одна якась помилка
      if (!errors.isEmpty()) {
        return next( ApiError.badRequest(`Помилка при валідації даних: ${JSON.stringify(errors.errors[0].msg)}`) );
      }

      const { email, password, name } = req.body;
      const userData = await userService.registration(email, password, name);

      // Зберігаємо refresh-токен в cookie
      res.cookie('refreshToken', userData.refreshToken,
                { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });

      return res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async login (req, res, next) {
    try {
      // Перевірка на валідність введених даних
      const errors = validationResult(req);

      // Якщо є хоча б одна якась помилка
      if (!errors.isEmpty()) {
        return next( ApiError.badRequest(`Помилка при валідації даних: ${JSON.stringify(errors.errors[0].msg)}`) );
      }

      const { email, password } = req.body;
      const userData = await userService.login(email, password);

      // Зберігаємо refresh-токен в cookie
      res.cookie('refreshToken', userData.refreshToken,
        { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });

      return res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async logout (req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const token = await userService.logout(refreshToken);

      // Видаляєм з cookie refreshToken
      res.clearCookie('refreshToken');

      return res.json(token);
    } catch (e) {
      next(e);
    }
  }
  
  async activate (req, res, next) {
    try {
      const activationLink = req.params.link;
      await userService.activate(activationLink);

      return res.redirect(process.env.CLIENT_URL);
    } catch (e) {
      next(e);
    }
  }
  
  async refresh (req, res, next) {
    try {
      const { refreshToken } = req.cookies;

      const userData = await userService.refresh(refreshToken);

      // Зберігаємо refresh-токен в cookie
      res.cookie('refreshToken', userData.refreshToken,
        { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });

      return res.json(userData);
    } catch (e) {
      next(e);
    } 
  }
  
  async getUsers (req, res, next) {
    try {
      return res.json( await userService.getAllUsers() );
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new UserController();