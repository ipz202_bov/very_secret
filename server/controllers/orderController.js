const orderService = require('../services/orderService');
const ApiError = require('../errors/apiError');
const {validationResult} = require("express-validator");

class OrderController {
  async toOrder (req, res, next) {
    try {
      // Перевірка на валідність введених даних
      const errors = validationResult(req);

      // Якщо є хоча б одна якась помилка
      if (!errors.isEmpty()) {
        return next( ApiError.badRequest(`Помилка при валідації даних: ${JSON.stringify(errors.errors[0].msg)}`) );
      }

      const { userId, phone, city, post, totalPrice } = req.body;
      const order = await orderService.toOrder(userId, phone, city, post, totalPrice);

      return res.json(order);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getOrdersForUser (req, res, next) {
    try {
      const { userId } = req.params;
      const orders = await orderService.getOrdersForUser(userId);

      return res.json(orders);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getOrdersForAdmin (req, res, next) {
    try {
      const orders = await orderService.getOrdersForUser(null, true);
      return res.json(orders);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async done (req, res, next) {
    try {
      const { orderId } = req.body;
      const result = await orderService.done(orderId);

      return res.json(result);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async deleteOrderUser (req, res, next) {
    try {
      const { orderId } = req.body;
      const result = await orderService.delete(orderId);

      return res.json(result);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new OrderController();