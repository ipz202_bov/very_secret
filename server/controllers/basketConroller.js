const basketService = require('../services/basketService');
const ApiError = require('../errors/apiError');

class BasketController {
  async create (req, res, next) {
    try {
      const { userId, goodsId } = req.body;
      const basketItem = await basketService.create(userId, goodsId);

      return res.json(basketItem);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async add (req, res, next) {
    try {
      const { userId, goodsId } = req.body;
      const basketItem = await basketService.add(userId, goodsId);

      return res.json(basketItem);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async sub (req, res, next) {
    try {
      const { userId, goodsId } = req.body;
      const basketItem = await basketService.sub(userId, goodsId);

      return res.json(basketItem);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async delete (req, res, next) {
    try {
      const { userId, goodsId } = req.body;

      const result = await basketService.delete(userId, goodsId);

      return res.json(result);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAll (req, res, next) {
    try {
      const { userId } = req.params;
      const goods = await basketService.getAll(userId);

      return res.json(goods);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new BasketController();