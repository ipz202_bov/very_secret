const ratingService = require('../services/ratingService');
const ApiError = require('../errors/apiError');

class RatingController {
  async post (req, res, next) {
    try {
      const { userId, goodsId, rate } = req.body;
      const rating = await ratingService.post(userId, goodsId, rate);

      return res.json(rating);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAvg (req, res, next) {
    try {
      const { goodsId } = req.params;
      const rate = await ratingService.getAverage(goodsId);

      return res.json( { rate: rate});
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getRate (req, res, next) {
    try {
      const { userId, goodsId } = req.query;
      const rate = await ratingService.getRate(userId, goodsId);

      return res.json(rate);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new RatingController();