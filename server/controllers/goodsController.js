const goodsService = require('../services/goodsService');
const ApiError = require('../errors/apiError');

class GoodsController {
  async create (req, res, next) {
    try {
      let { name, price, brandId, typeId, info, count } = req.body;
      const { img } = req.files;

      const goods = await goodsService.create(name, price, brandId, typeId, info, count, img);
      return res.json(goods);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async update (req, res, next) {
    try {
      let { name, price, brandId, typeId, info, count, id } = req.body;

      let img = null;
      if (req.files) {
       img = req.files.img;
      }

      const goods = await goodsService.update(name, price, brandId, typeId, info, count, img, id);
      return res.json(goods);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async delete (req, res, next) {
    try {
      const { id } = req.body;
      const type = await goodsService.delete(+id);
      return res.json(type);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAll (req, res) {
    let { brandId, typeId, limit, page, search } = req.query;

    page = page || 1;
    limit = limit || 8;
    let offset = page * limit - limit;

    const goods = await goodsService.getAll(brandId, typeId, limit, offset, search);

    return res.json(goods);
  }

  async getOne (req, res, next) {
    try {
      const { id } = req.params;
      const goods = await goodsService.getOne(id);

      return res.json(goods);
    } catch (e) {
     next( ApiError.badRequest(e.message) )
    }
  }

  async getAllForAdmin (req, res, next) {
    try {
      const goods = await goodsService.getAllForAdmin();

      return res.json({rows: goods});
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new GoodsController();