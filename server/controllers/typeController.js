const typeService = require('../services/typeService');
const ApiError = require('../errors/apiError');

class TypeController {
  async create (req, res, next) {
    try {
      const { name } = req.body;
      const type = await typeService.create(name);

      return res.json(type);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async update (req, res, next) {
    try {
      const { name, id, cName} = req.body;
      const type = await typeService.update(name, id, cName);

      return res.json(type);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async delete (req, res, next) {
    try {
      const { id } = req.body;
      const type = await typeService.delete(+id);
      return res.json(type);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAll (req, res, next) {
    try {
      const types = await typeService.getAll();

      return res.json(types);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new TypeController();