const brandService = require('../services/brandService');
const ApiError = require('../errors/apiError');

class BrandController {
  async create (req, res, next) {
    try {
      const { name } = req.body;
      const brand = await brandService.create(name);

      return res.json(brand);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async update (req, res, next) {
    try {
      const { name, id, cName} = req.body;

      const brand = await brandService.update(name, id, cName);

      return res.json(brand);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async delete (req, res, next) {
    try {
      const { id } = req.body;
      const type = await brandService.delete(id);

      return res.json(type);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAll (req, res, next) {
    try {
      const brands = await brandService.getAll();

      return res.json(brands);
    } catch (e) {
     next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new BrandController();