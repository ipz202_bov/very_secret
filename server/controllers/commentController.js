const commentService = require('../services/commentService');
const ApiError = require('../errors/apiError');

class CommentController {
  async post (req, res, next) {
    try {
      const { userId, goodsId, comment } = req.body;
      const rating = await commentService.post(userId, goodsId, comment);

      return res.json(rating);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }

  async getAll (req, res, next) {
    try {
      const { goodsId } = req.params;
      const comments = await commentService.getAll(goodsId);

      return res.json(comments);
    } catch (e) {
      next( ApiError.badRequest(e.message) );
    }
  }
}

module.exports = new CommentController();