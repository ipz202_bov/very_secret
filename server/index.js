// Підключення встановлених бібліотек
require('dotenv').config();
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const path = require('path');

// Підключення власних модулів
const sequelize = require('./db');
const router = require('./routes/index');
const errorHandler = require('./middlewares/errorHandlingMiddleware');
const PORT = process.env.PORT || 5000;

// Створюємо об'єкт додатку
const app = express();

app.use( express.json() );
app.use( express.static(path.resolve(__dirname, 'static')) );
app.use( fileUpload( {} ) );
app.use( cookieParser() );
app.use( cors( {credentials: true, origin: process.env.CLIENT_URL} ) );
app.use('/api', router);

app.use(errorHandler);

// Функція для запуску додатку
const start = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync();

    app.listen(PORT, () => console.log(`Сервер запущено на порту ${PORT}!`) );
  } catch (e) {
    console.log(e);
  }
}

// Викликаємо функцію запуску додатку
start();