const ApiError = require('../errors/apiError');
const tokenService = require('../services/tokenService');

module.exports = (req, res, next) => {
  if (req.method === "OPTIONS") {
    next();
  }

  try {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
      return next( ApiError.unauthorized(`Користувач не авторизований!`) );
    }

    const accessToken = authHeader.split(' ')[1];

    if (!accessToken) {
      return next( ApiError.unauthorized(`Користувач не авторизований!`) );
    }

    const userData = tokenService.validateAccessToken(accessToken);
    if (!userData) {
      return next( ApiError.unauthorized(`Користувач не авторизований!`) );
    }

    req.user = userData;
    next();
  } catch (e) {
    next( ApiError.unauthorized(`Користувач не авторизований!`) );
  }
}