const ApiError = require('../errors/apiError');
const tokenService = require('../services/tokenService');

module.exports = (role) => {
  return (req, res, next) => {
    if (req.method === "OPTIONS") {
      next();
    }

    try {
      const authHeader = req.headers.authorization;

      if (!authHeader) {
        return next( ApiError.unauthorized(`Користувач не авторизований!`) );
      }

      const accessToken = authHeader.split(' ')[1];

      if (!accessToken) {
        return next( ApiError.unauthorized(`Користувач не авторизований!`) );
      }

      const userData = tokenService.validateAccessToken(accessToken);
      if (!userData) {
        return next( ApiError.unauthorized(`Користувач не авторизований!`) );
      }

      if (userData.role !== role) {
          return next ( ApiError.forbidden(`Немає доступу!`) );
      }

      req.user = userData;
      next();
    } catch (e) {
      next( ApiError.unauthorized(`Користувач не авторизований!`) );
    }
  }
}