const Basket = require('./basketModel');
const User = require('./userModel');
const BasketGoods = require('./basketGoodsModel');
const Brand = require('./brandModel');
const Comment = require('./сommentModel');
const Goods = require('./goodsModel');
const GoodsInfo = require('./goodsInfoModel');
const Rating = require('./ratingModel');
const Type = require('./typeModel');
const TypeBrand = require('./typeBrandModel');
const Token = require('./tokenModel');
const Order = require('./OrderModel');
const OrderGoods = require('./OrderGoodsModel');

User.hasOne(Basket);
Basket.belongsTo(User);

User.hasOne(Token);
Token.belongsTo(User);

User.hasMany(Order);
Order.belongsTo(User);

Order.hasMany(OrderGoods);
OrderGoods.belongsTo(Order);

Goods.hasMany(OrderGoods);
OrderGoods.belongsTo(Goods);

User.hasMany(Rating);
Rating.belongsTo(User);

User.hasMany(Comment);
Comment.belongsTo(User);

Goods.hasMany(Comment);
Comment.belongsTo(Goods);

Basket.hasMany(BasketGoods);
BasketGoods.belongsTo(Basket);

Type.hasMany(Goods);
Goods.belongsTo(Type);

Brand.hasMany(Goods);
Goods.belongsTo(Brand);

Goods.hasMany(Rating);
Rating.belongsTo(Goods);

Goods.hasMany(GoodsInfo, { as: 'info' });
GoodsInfo.belongsTo(Goods);

Goods.hasMany(BasketGoods);
BasketGoods.belongsTo(Goods);

Type.belongsToMany(Brand, {through: TypeBrand});
Brand.belongsToMany(Type, {through: TypeBrand});

module.exports = { User, Basket, BasketGoods, Goods, GoodsInfo,
  Type, Brand, Rating, TypeBrand, Comment, Token, Order, OrderGoods};