const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const GoodsInfo = sequelize.define('goods_info', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  title: {type: DataTypes.STRING, allowNull: false},
  description: {type: DataTypes.STRING, allowNull: false}
});

module.exports = GoodsInfo;