const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const Order = sequelize.define('order', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  phone: {type: DataTypes.STRING, allowNull: false},
  city: {type: DataTypes.STRING, allowNull: false},
  post: {type: DataTypes.STRING, allowNull: false},
  totalPrice: {type: DataTypes.INTEGER,  allowNull: false},
  isDone: { type: DataTypes.BOOLEAN, defaultValue: false }
});

module.exports = Order;