const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const Order = sequelize.define('order_goods', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  count: {type: DataTypes.INTEGER, defaultValue: 1 },
});

module.exports = Order;