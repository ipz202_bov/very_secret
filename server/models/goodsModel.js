const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const Goods = sequelize.define('goods', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  name: {type: DataTypes.STRING, unique: true, allowNull: false},
  price: {type: DataTypes.INTEGER, allowNull: false},
  count: { type: DataTypes.INTEGER, defaultValue: 1},
  rating: {type: DataTypes.FLOAT, defaultValue: 0},
  img: {type: DataTypes.STRING, allowNull: false},
});

module.exports = Goods;