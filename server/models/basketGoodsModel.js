const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const BasketGoods = sequelize.define('basket_goods', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  count: {type: DataTypes.INTEGER, defaultValue: 1 },
});

module.exports = BasketGoods;