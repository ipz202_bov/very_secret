const sequelize = require("../db");
const { DataTypes } = require('sequelize');

const Comment = sequelize.define('comment', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false},
  text: {type: DataTypes.STRING, allowNull: false}
});

module.exports = Comment;
