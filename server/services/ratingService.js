const { Rating, Goods } = require('../models/models');

class RatingService {
  async post (userId, goodsId, rate) {
    let applicant = await Rating.findOne( { where: { userId: userId, goodId: goodsId } } );

    if (applicant) {
      if (rate > 100) {
        applicant.rate = 100;
      } else if (rate < 1) {
        applicant.rate = 0;
      } else {
        applicant.rate = rate;
      }

      await applicant.save();
    } else {
      applicant = await Rating.create( { goodId: goodsId, rate, userId } );
    }

    const goods = await Goods.findByPk(goodsId);
    goods.rating = await this.getAverage(goodsId);
    await goods.save();

    return { applicant, goods };
  }

  async getRate (userId, goodsId) {
    return await Rating.findOne( { where: { userId, goodId: goodsId } } );
  }

  async getAverage (goodsId) {
    const rates = await Rating.findAll( { where: { goodId: goodsId } } );

    let average = 0;

    if (!rates) {
      return average;
    }

    rates.forEach( rate => average += rate.rate );
    average /= rates.length;

    average = Math.round(average, 1);

    return average;
  }
}

module.exports = new RatingService();