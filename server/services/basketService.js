const { Goods, Basket, BasketGoods, User } = require('../models/models');
const ApiError = require("../errors/apiError");

class BasketService {
  async create (userId, goodsId) {
    const basket = await Basket.findOne( { where: { userId }} );

    const applicant = await BasketGoods.findOne( { where: { goodId: goodsId, basketId: basket.id } } );
    if (applicant) {
      throw ApiError.badRequest(`Помилка! Такий товар уже створено!`);
    }

    return await BasketGoods.create( { count: 1, goodId: goodsId, basketId: basket.id  } );
  }

  async add (userId, goodsId) {
    const basket = await Basket.findOne( { where: { userId }} );
    const goods = await Goods.findByPk(goodsId);

    const applicant = await BasketGoods.findOne( { where: { goodId: goodsId, basketId: basket.id } } );
    if (!applicant) {
      throw ApiError.badRequest(`Помилка! Товару в корзині не існує`);
    }

    applicant.count += 1;
    goods.count -= 1;

    if (goods.count < 0) {
      throw ApiError.badRequest(`Помилка! Не вистачає товару на складі! Він не може бути від'ємним!`);
    }

    await applicant.save();
    await goods.save();

    return { applicant, goods };
  }

  async sub (userId, goodsId) {
    const basket = await Basket.findOne( { where: { userId }} );
    const goods = await Goods.findByPk(goodsId);

    const applicant = await BasketGoods.findOne( { where: { goodId: goodsId, basketId: basket.id } } );
    if (!applicant) {
      throw ApiError.badRequest(`Помилка! Товару в корзині не існує`);
    }

    applicant.count -= 1;
    if (applicant.count <= 0) {
      throw ApiError.badRequest(`Помилка! К-сть товару не може бути менше чим один!`);
    }

    goods.count += 1;

    await applicant.save();
    await goods.save();

    return { applicant, goods };
  }

  async delete (userId, goodsId) {
    const basket = await Basket.findOne( { where: { userId }} );

    const applicant = await BasketGoods.findOne( { where: { goodId: goodsId, basketId: basket.id } } );
    if (!applicant) {
      throw ApiError.badRequest(`Помилка! Товару в корзині не існує`);
    }

    await applicant.destroy();
    return { message: 'ТОВАР УСПІШНО ВИДАЛЕНО!' }
  }

  async getAll (userId) {
    const basket = await Basket.findOne( { where: { userId }} );
    const goodsArr = await BasketGoods.findAll( { where: { basketId: basket.id } } );

    let resultArray = [];

    // Якщо в корзині пусто, то вертаємо пустий масив
    if (!goodsArr) {
      return resultArray;
    }

    let totalPrice = 0;
    for (const good of goodsArr) {
      const product = await Goods.findByPk(good.goodId);
      const count = await good.count;
      totalPrice += product.price * count
      resultArray.push( { product, count } );
    }

    return { resultArray, totalPrice };
  }
}

module.exports = new BasketService();