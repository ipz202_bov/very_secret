const { Brand, Goods} = require('../models/models');
const ApiError = require("../errors/apiError");

class TypeService {
  async create (name) {
    return await Brand.create( {name} )
  }

  async delete (id) {
    const brand = await Brand.findByPk(id);

    const goods = await Goods.findAndCountAll( { where: { brandId: id } } );

    if (goods.count > 0) {
      throw ApiError.badRequest(`Помилка! Спочатку видаліть всі товари цього бренду!`);
    }

    await brand.destroy();
    return {message: 'Обраний бренд видалено!'}
  }

  async update (name, id, cName) {
    let brand;

    if (!name) {
      throw ApiError.badRequest(`Помилка! Нова назва бренду некоректна`)
    }

    if (id) {
      brand = await Brand.findByPk(id);
    } else if (cName) {
      brand = await Brand.findOne( { where: { name: cName } } );
    } else {
      throw ApiError.badRequest(`Помилка! У переданих параметрах не знайдено ні ід ні імені!`);
    }

    if (!brand) {
      throw ApiError.badRequest(`Помилка! Нова назва не може бути пустою!`);
    }

    brand.name = name;
    await brand.save();

    return brand;
  }

  async getAll () {
    return await Brand.findAll();
  }
}

module.exports = new TypeService();