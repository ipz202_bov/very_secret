const { Goods, GoodsInfo, Brand, Type} = require('../models/models');
const fs = require('fs')
const uuid = require("uuid");
const path = require("path");

class GoodsService {
  async create (name, price, brandId, typeId, info, count, img) {
    let fileName = `${uuid.v4()}.jpg`;
    img.mv( path.resolve(__dirname, '..', 'static', fileName) );

    const goods = await Goods.create( { name, price, brandId, typeId, img: fileName, count } );

    if (info) {
      info = JSON.parse(info);
      info.forEach(i => {
        GoodsInfo.create( { title: i.title, description: i.description, goodId: goods.id } );
      });
    }

    return goods;
  }

  async update (name, price, brandId, typeId, info, count, img, id) {
    id = parseInt(id);
    const goods = await Goods.findByPk(id);
    goods.name = name;
    goods.price = price;
    goods.count = count;

    if (img) {
      // Оновлена картинка
      let fileName = `${uuid.v4()}.jpg`;
      img.mv( path.resolve(__dirname, '..', 'static', fileName) );
      goods.img = fileName;

      // Видалення старої картинки
      fs.unlink( path.resolve(__dirname, '..', 'static', goods.img), err => {
        console.log(err);
      } );
    }

    const brand = await Brand.findOne( { where : { name: brandId }} );
    const type = await Type.findOne( { where: { name: typeId } } );
    goods.brandId = brand.id;
    goods.typeId = type.id;

    await goods.save();

    if (info) {
      info = JSON.parse(info);
      for (const i of info) {
        const infoItem = await GoodsInfo.findByPk(i.id);
        infoItem.title = i.title;
        infoItem.description = i.description;
        await infoItem.save();
      }
    }

    return goods;
  }

  async delete (id) {
    const goods = await Goods.findByPk(id);

    fs.unlink( path.resolve(__dirname, '..', 'static', goods.img), err => {
      console.log(err);
    } );

    // Видалення характеристик
    const infos = await GoodsInfo.findAll( { where: { goodId: id } } );
    infos.map( async info => await info.destroy());

    await goods.destroy();
    return {message: 'Обраний товар видалено!'}
  }

  async getAll (brandId, typeId, limit, offset, search) {
    let goods;
    if (search) {
      goods = await Goods.findAndCountAll( { where: { name: search } } );
      return goods;
    }

    if (!brandId && !typeId) {
      goods = await Goods.findAndCountAll( { limit, offset } );
    }

    if (!brandId && typeId) {
      goods = await Goods.findAndCountAll( { where: { typeId }, limit, offset } );
    }

    if (!typeId && brandId) {
      goods = await Goods.findAndCountAll( { where: { brandId }, limit, offset } );
    }

    if (typeId && brandId) {
      goods = await Goods.findAndCountAll( { where: {typeId, brandId}, limit, offset } );
    }

    const compare = (a, b) => {
      if (a.id > b.id)
        return -1;
      if (a.id < b.id)
        return 1;
      return 0;
    }

    goods.rows = goods.rows.sort(compare);

    return goods;
  }

  async getAllForAdmin ()  {
    return await Goods.findAll();
  }

  async getOne (id) {
    return await Goods.findOne({
      where: {id},
      include: [ { model: GoodsInfo, as: 'info'} ]
    });
  }

}

module.exports = new GoodsService();