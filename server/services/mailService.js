const mailer = require('nodemailer');

class MailService {
  constructor() {
    this.transporter = mailer.createTransport( {
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: false,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD
      }
    });
  }

  // Функція відправдення листа з підтвердженням реєстрації
  async sendActivationMail (to, link) {
    await this.transporter.sendMail( {
      from: process.env.SMTP_USER,
      to,
      subject: `Активація акаунту на сайті: ${process.env.API_URL}`,
      text: '',
      html:
          `
            <div>
              <h1> Для активації акаунту, будь ласка, перейдіть за посиланям:</h1>
              <img style="height: 300px" 
              src="https://fainaidea.com/wp-content/uploads/2021/12/goal-soccer.jpg" alt="Фото магазину">
              <div>
               <a href="${link}">${link}</a>
              </div>
            </div>
          `
    });
  }
}

module.exports = new MailService();