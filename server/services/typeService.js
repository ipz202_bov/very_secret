const { Type, Goods } = require('../models/models');
const ApiError = require("../errors/apiError");

class TypeService {
  async create (name) {
    if (!name) {
      throw ApiError.badRequest(`Помилка! Некоректна назва!`);
    }

    return await Type.create( {name} )
  }

  async delete (id) {
    const type = await Type.findByPk(id);

    const goods = await Goods.findAndCountAll( { where: { typeId: id } } );

    if (goods.count > 0) {
      throw ApiError.badRequest(`Помилка! Спочатку видаліть всі товари цього типу!`);
    }

    await type.destroy();
    return {message: 'Обраний тип видалено!'}
  }

  async update (name, id, cName) {
    let type;

    if (name) {
      throw ApiError.badRequest(`Помилка! Нова назва типу некоректна!`)
    }

    if (id) {
      type = await Type.findByPk(id);
    } else if (cName) {
      type = await Type.findOne( { where: { name: cName } } );
    } else {
      throw ApiError.badRequest(`Помилка! У переданих параметрах не знайдено ні ід ні імені!`);
    }

    if (!name) {
      throw ApiError.badRequest(`Помилка! Нова назва не може бути пустою!`);
    }

    type.name = name;
    await type.save();

    return type;
  }

  async getAll () {
    return await Type.findAll();
  }
}

module.exports = new TypeService();