const { Token } = require('../models/models');
const jwt = require('jsonwebtoken');

class TokenService {
  generateTokens (payload) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET_KEY, { expiresIn: '10m' });
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET_KEY, { expiresIn: '30d' });

    return { accessToken, refreshToken };
  }

  validateAccessToken (token) {
    try {
      const user = jwt.verify(token, process.env.JWT_ACCESS_SECRET_KEY);
      return user;
    } catch (e) {
      return null;
    }
  }

  validateRefreshToken (token) {
    try {
      const user = jwt.verify(token, process.env.JWT_REFRESH_SECRET_KEY);
      return user;
    } catch (e) {
      return null;
    }
  }

  // Функція, яка видаляє токен
  async removeToken (refreshToken) {
    return await Token.destroy( { where: { refreshToken } } );
  }

  // Функція, яка шукає токен
  async findToken (refreshToken) {
    return await Token.findOne( { where: { refreshToken } } );
  }

  // Функція, яка зберігає refreshToken, у БД, у таблиці token
  async saveRefreshToken (userId, refreshToken) {
    const token = await Token.findOne( { where: { userId: userId } } );

    // Якщо токен уже є, то просто перезаписуємо його
    if (token) {
      token.refreshToken = refreshToken;
      return token.save();
    }

    return await Token.create({userId: userId, refreshToken});
  }
}

module.exports = new TokenService();