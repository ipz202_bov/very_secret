const { Basket, BasketGoods, OrderGoods, Order, Goods} = require('../models/models');

class OrderService {
  async toOrder (userId, phone, city, post, totalPrice) {
    const basket = await Basket.findOne( { where: { userId: userId} } );
    const goods = await BasketGoods.findAll( { where: { basketId: basket.id } } );

    const order = await Order.create( { userId: userId, totalPrice, city, post, phone } );

    goods.forEach( async (good) => {
      await OrderGoods.create( { goodId: good.goodId, orderId: order.id, count: good.count } );
      await good.destroy();
    } );

    return { order, goods }
  }

  async delete (orderId) {
    const order = await Order.findByPk(orderId);

    const goods = await OrderGoods.findAll( { where: { orderId: order.id } } );

    for (const good of goods) {
      // Повертаємо на склад взяту к-сть товару та зберігаємо
      const goodInStore = await Goods.findByPk(good.goodId);
      goodInStore.count += good.count;
      await goodInStore.save();
      await good.destroy();
    }

    await order.destroy();

    return { message: 'Замовлення та товари у ньому видалено!' }
  }

  async done (orderId) {
    const order = await Order.findByPk(orderId);
    order.isDone = true;
    await order.save();

    return order;
  }

  async getOrdersForUser (userId, isAdmin = false) {
    let orders;
    isAdmin ?  orders =  await Order.findAll()
            : orders =  await Order.findAll( { where: { userId: userId} });

    let resultArray = [];

    for (let i = 0; i < orders.length; i++){
      const order = orders[i];
      const goods = await OrderGoods.findAll( { where: { orderId: order.id } } );

      let goodsArray = [];

      for (const good of goods) {
        const goodsItem = await Goods.findByPk(good.goodId);

        goodsArray.push( { count: good.count, name: goodsItem.name } );
      }

      resultArray.push( { order: order, goods: goodsArray } );
    }

    return { resultArray };
  }
}

module.exports = new OrderService();