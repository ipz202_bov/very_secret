const { User, Comment } = require('../models/models');
const ApiError = require("../errors/apiError");

class CommentService {
  async post(userId, goodsId, comment) {
    if (!comment) {
      throw ApiError.badRequest('Помилка! Коментар не може бути пустим')
    }

    return await Comment.create({userId: userId, text: comment, goodId: goodsId});
  }

  async getAll(goodsId) {
    const comments = await Comment.findAll({where: {goodId: goodsId}});

    let resultArray = [];

    for (const comment of comments) {
      const user = await User.findByPk(comment.userId);
      resultArray.push({user: user.name, comment: comment});
    }

    return resultArray;
  }
}

module.exports = new CommentService();