const { User, Basket } = require('../models/models');
const UserDto = require('../dtos/userDto');
const mailService = require('./mailService');
const tokenService = require('./tokenService');
const ApiError = require('../errors/apiError');

const bcrypt = require('bcrypt');
const uuid = require('uuid');
const {where} = require("sequelize");

class UserService {
  // Функція, для реєстрації користувача
  async registration (email, password, name) {
    const applicant = await User.findOne( { where: { email } } );

    if (applicant) {
      throw ApiError.badRequest(`Користувач з поштовою скринькою "${email}" уже є у базі даних!`);
    }

    const hashPassword = await bcrypt.hash(password, 4);
    const activationLink = uuid.v4();
    const user = await User.create( { email: email, name: name,
                                            password: hashPassword, activationLink: activationLink } );

    // Створюємо кошик для користувача. Зв'язок 1 до 1
    await Basket.create( { userId: user.id } );

    await mailService.sendActivationMail(email,
      `${process.env.API_URL}/api/user/activate/${activationLink}`);

    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens( {...userDto} );
    await tokenService.saveRefreshToken(userDto.id, tokens.refreshToken);

    return { ...tokens, user: userDto };
  }

  // Функція для підтвердження реєстрації користувача
  async activate (activationLink) {
    const user = await User.findOne( { where: { activationLink: activationLink } } );

    // Якщо користувача з таким посиланням в БД немає, то генеруємо помилку
    if (!user) {
      throw ApiError.badRequest(`Неправильне посилання для активації!`);
    }

    // Якщо такий юзер є, то відмічаємо в базі, що акаунт активовано та зберігаємо зміни в базі
    user.isActivated = true;
    await user.save();
  }

  // Функція для логінації користувача
  async login (email, password) {
    const user = await User.findOne( { where: { email } } );

    // Якщо користувача не знайдено, то прокидуємо помилку
    if (!user) {
      throw ApiError.badRequest(`Користувача з поштовою скринькою ${email} у базі даних не знайдено!`);
    }

    // Інакше зрівнюємо хеші паролів
    const isPasswordRight = await bcrypt.compare(password, user.password);

    // Якщо хеші не співпадають, то прокидуємо помилку
    if (!isPasswordRight) {
      throw ApiError.badRequest(`Введений пароль не співпадає! Пароль некорректний!`);
    }

    const userDto = new UserDto(user);

    // Якщо користувач неактивований
    if (!userDto.isActivated) {
      throw ApiError.unauthorized(`Пошта не активована! Спочатку активуйте пошту!`);
    }

    const tokens = tokenService.generateTokens( { ...userDto } );

    await tokenService.saveRefreshToken(userDto.id, tokens.refreshToken);

    return { ...tokens, user: userDto };
  }

  async logout (refreshToken) {
    return await tokenService.removeToken(refreshToken);
  }

  async refresh (refreshToken) {
    if (!refreshToken) {
      throw ApiError.unauthorized(`Користувач не авторизований!`);
    }

    const userData = tokenService.validateRefreshToken(refreshToken);
    const dbToken = await tokenService.findToken(refreshToken);

    if (!userData || !dbToken) {
      throw ApiError.unauthorized(`Користувач не авторизований!`);
    }

    const user = await User.findByPk(userData.id);
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens( { ...userDto } );

    await tokenService.saveRefreshToken(userDto.id, tokens.refreshToken);

    return { ...tokens, user: userDto };
  }

  async getAllUsers () {
    return await User.findAll( { where: { role: `USER` } } );
  }

}

module.exports = new UserService();