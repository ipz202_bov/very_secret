const Router = require('express');
const router = new Router();

const ratingController = require('../controllers/ratingController');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/', checkRoleMiddleware(`USER`), ratingController.post);
router.get('/:goodsId', ratingController.getAvg);
router.get(`/`, ratingController.getRate)

module.exports = router;