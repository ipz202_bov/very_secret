const Router = require('express');
const router = new Router();

const typeController = require('../controllers/typeController');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/', checkRoleMiddleware(`ADMIN`), typeController.create);
router.post('/delete', checkRoleMiddleware(`ADMIN`), typeController.delete);
router.post('/update', checkRoleMiddleware(`ADMIN`), typeController.update);
router.get('/', typeController.getAll);

module.exports = router;