const Router = require('express').Router;
const router = new Router();

const goodsRouter = require('./goodsRouter');
const brandRouter = require('./brandRouter');
const typeRouter = require('./typeRouter');
const userRouter = require('./userRouter');
const basketRouter = require('./basketRouter');
const commentRouter = require('./commentRouter');
const ratingRouter = require('./ratingRouter');
const orderRouter = require('./orderRouter');

router.use('/user', userRouter);
router.use('/brand', brandRouter);
router.use('/type', typeRouter);
router.use('/goods', goodsRouter);
router.use('/basket', basketRouter);
router.use('/comment', commentRouter);
router.use('/rating', ratingRouter);
router.use('/order', orderRouter);

module.exports = router;