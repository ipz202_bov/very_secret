const Router = require('express');
const router = new Router();

const basketController = require('../controllers/basketConroller');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/', checkRoleMiddleware(`USER`), basketController.create);
router.post('/delete', checkRoleMiddleware(`USER`), basketController.delete);
router.post('/add', checkRoleMiddleware(`USER`), basketController.add);
router.post('/sub', checkRoleMiddleware(`USER`), basketController.sub);

router.get('/:userId', checkRoleMiddleware(`USER`), basketController.getAll);

module.exports = router;