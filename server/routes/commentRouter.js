const Router = require('express');
const router = new Router();

const commentController = require('../controllers/commentController');

router.post('/', commentController.post);
router.get('/:goodsId', commentController.getAll);

module.exports = router;