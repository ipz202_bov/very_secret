const { body } = require('express-validator');
const Router = require('express');
const router = new Router();

const orderController = require('../controllers/orderController');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/', checkRoleMiddleware(`USER`),
  body('phone').isLength( { min: 9 } ),
  body('city').isLength( { min: 3 } ),
  body('post').isLength({min: 3 }),
  orderController.toOrder);

router.post('/delete/', checkRoleMiddleware(`USER`), orderController.deleteOrderUser);
router.get('/all', checkRoleMiddleware(`ADMIN`), orderController.getOrdersForAdmin);
router.post('/done', checkRoleMiddleware(`ADMIN`), orderController.done);
router.get('/:userId', checkRoleMiddleware(`USER`), orderController.getOrdersForUser);

module.exports = router;