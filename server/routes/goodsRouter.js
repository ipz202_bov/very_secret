const Router = require('express');
const router = new Router();

const goodsController = require('../controllers/goodsController');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/',checkRoleMiddleware(`ADMIN`),  goodsController.create);
router.post('/delete', checkRoleMiddleware(`ADMIN`), goodsController.delete);
router.post('/update', checkRoleMiddleware(`ADMIN`), goodsController.update);
router.get('/', goodsController.getAll);
router.get('/getAdmin', checkRoleMiddleware(`ADMIN`), goodsController.getAllForAdmin)
router.get('/:id', goodsController.getOne);

module.exports = router;