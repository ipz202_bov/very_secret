const Router = require('express');
const router = new Router();

const brandController = require('../controllers/brandController');

const checkRoleMiddleware = require('../middlewares/roleMiddleware');

router.post('/', checkRoleMiddleware(`ADMIN`), brandController.create);
router.post('/delete', checkRoleMiddleware(`ADMIN`), brandController.delete);
router.post('/update', checkRoleMiddleware(`ADMIN`), brandController.update);
router.get('/', brandController.getAll);

module.exports = router;