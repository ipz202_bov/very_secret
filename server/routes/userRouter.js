const { body } = require('express-validator');
const Router = require('express');
const router = new Router();
const roleMiddleware = require('../middlewares/roleMiddleware');
const userController = require('../controllers/userController');

router.post('/registration',
  body('email').isEmail(),
  body('password').isLength( { min: 6, max: 16 } ),
  body('name').isLength({min: 3, max: 20}),
  userController.registration);

router.post('/login',
  // body('email').isEmail(),
  // body('password').isLength( { min: 6, max: 16 } ),
  // userController.registration,
  userController.login);

router.post('/logout', userController.logout);
router.get('/activate/:link', userController.activate);
router.get('/refresh', userController.refresh);

router.get('/users', roleMiddleware(`ADMIN`), userController.getUsers);

module.exports = router;