module.exports = class UserDto {
  id;
  email;
  role;

  constructor(userModel) {
    this.id = userModel.id;
    this.isActivated = userModel.isActivated;
    this.email = userModel.email;
    this.role = userModel.role;
  }
}